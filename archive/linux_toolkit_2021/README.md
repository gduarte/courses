# Linux Toolbox

Linux is well-known for being configurable and fitting many different styles of use. But what can you do with it?
Our goal in the Linux Toolbox course is to introduce a large range of tools and to teach you how you can profit from them. We'll show you typical tasks that Linux is good at, such as renaming hundreds of files, searching and replacing text, automatic downloading etc.. This way, you'll be able to recognize those tasks in the wild and don't have to do them by hand.

Linux has many useful tools, so we don't intend to cover them all in detail. We'll instead take a sweeping glance to cover as many use cases as possible and provide you with learning resources if you want to delve deeper into a topic. Most of the course will take place on the command line, but no prerequisite knowledge is required.
