# Git Cheat Sheet

## Setting up Git for the first time 

#### 1. Set your identity

```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```
#### 2. Optionally:

check your settings:
```
$ git config --list         
```
Change your standard editor:
```
$ git config --global core.editor emacs
```
change default branch name: 
```
$ git config --global init.defaultBranch main
```
   
#### using --local will apply changes only to git repo you are currently in

## Setting up SSH key

#### 1. Generate Key

```
cd ~/.ssh
ssh-keygen -t ed25519 -C "your_email@example.com"
```
Choose a sensible name as KEYNAME  
(Optionally use a passphrase(password) for your Key. Or Skip with Enter.)  

#### 2. Start ssh-agent

```
$ eval "$(ssh-agent -s)"
```
#### 3. Add key to agent 


```
ssh-add ~/.ssh/KEYNAME

```

#### 4. Add key online to your Gitlab account:   

Copy the content of KEYNAME.pub   

[https://gitlab.ethz.ch/-/profile/keys](https://gitlab.ethz.ch/-/profile/keys)  

#### <span style="color:red">Never Share your private Key(KEYNAME)</span>   


## After Creating a Project online 

Choose one of the both options to start working on your Project locally

### Clone Project
 
```
cd desired_location
git clone git@gitlab.ethz.ch:USER_NAME/PROJECT_NAME.git
cd ./PROJECT_NAME
```

### Connect to existing Project Folder

```
cd existing_repo
git init
git remote add origin https://gitlab.ethz.ch/USER_NAME/PROJECT_NAME.git
git branch -M main
git push -uf origin main
```

## Git Workflow

See your current staged and unstage files

```
git status
```

Look at your Commit history

```
git log
```

### Commit

```
git add * 
git commit -m "Commit Messages" -m "optional commit Description"
git pull
git push
```

### Reset (This does not Change your files)

Find out the Commit name of the Commit that you want to change 
```
git log
```
Commit Name looks something like this 
```
commit d69d6463e460c458ae77da6d969420f4fcf09146 (HEAD -> master)
```

Take at least the first seven characters from the name  (in our case d69d646)

```
git reset d69d646 
```
Lifehacks
```
git reset filename //unstage changes to certain file
git reset HEAD     //remove all files from staging
git reset HEAD^N   // unstage last N commits
```

### Working with branches

creating, deleting and renaming branches

```
git branch             //list all of your branches and which one you are currently on
git branch NAME     //create new branch
git branch -D NAME     // delete branch (dosnt work if you have unmerged changes)
git branch -d NAME      // delete branch NAME
git branch -m NAME     // rename current branch to NAME
```
switching between branches

```
git checkout NAME //switch to branch NAME
git switch Name   // does the same 
git checkout -b NAME //creates and switches to new branch NAME
git switch -c NAME // does the same 
```
Merging braches 

```
git merge NAME //merges NAME into current branch
```

#### git checkout can also be used to go to past commits. However if you wanna work on past versions you need to create a new branch from  there.
