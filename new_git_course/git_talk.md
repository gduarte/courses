---
author:
- Fadri Lardon
title: Git and Gitlab
---
 

# Introduction

## Who are we?
\includegraphics[width=\textwidth]{img/logo_blue.pdf}

## What git?
* THE modern distributed version control system
* originally written to develop Linux (the Kernel you're probably using)

## Why git?
* change history with the ability to undo almost any operation
* the ability work with a remote repository
* easy to collaborate with others and yourself
* ensure file integrity
* branches

## Where git?
* github
* linux
* literally like almost every open (or closed)- source project
* books are written this way
* this talk is managed with git

## How git?
That's why you're here!

# Basic Concepts
##
\bigtext{Basic Concepts}

## Terminology
### commit
represents a snapshot of your project this is the smallest unit of change that git records

### branch
a chain of commits under a specific name

### repository
the structure in which git stores your data, effectively your project folder

## Repository


\begin{tikzpicture}
\draw[line width=.5pt]
    (4, 3) node[draw] {Repository}
    (-1,0)node[draw] {main}
    (-1,1)node[draw] {auxiliary}
    (-1,-1)node[draw] {other}
    (0,0)node[draw, circle] (0) {}
    (1,0)node[draw, circle] (1) {}
    (2,0)node[draw, circle] (2) {}
    (3,1)node[draw, circle] (3) {}
    (3,0)node[draw, circle] (4) {}
    (4,1)node[draw, circle] (5) {}
    (4,0)node[draw, circle] (6) {}
    (5,0)node[draw, circle] (7) {}
    (4,-1)node[draw, circle] (8) {}
    (6,0)node[draw, circle] (9) {}
    (5,-1)node[draw, circle] (10) {}
    [-] (0) -- (1) -- (2) -- (4) -- (6) -- (7) -- (9)
    [-] (2) -- (3) -- (5) -- (7)
    [-] (4) -- (8) -- (10)
    ;
\end{tikzpicture}

## States of Git

\begin{tikzcd}[ampersand replacement=\&]
    \text{working directory} \ar[rr, out=50, in=130, "\text{git add}"] \& \&  \text{staging area} \ar[ld, bend left, "\text{git commit}"] \\
    \& \text{repository}
    \ar[lu, bend left, "\text{git checkout}"]
\end{tikzcd}


## Git Workflow

\begin{tikzcd}[ampersand replacement=\&]
    \text{local repository} \ar[d, bend right, "\text{change files}"] \ar[dr, bend left, in=160, out=20, "\text{create files}"] \\
    \text{unstaged changes} \ar[d, bend right, "\text{commit changes}"] \&  \text{untracked files} \ar[dl, bend left, out=10, in=170,  "\text{add to repo}"] \& \text{remote repository} \ar[llu, bend right, dashed,  "\text{pull}"] \\
    \text{local commits} \ar[rru, bend right, dashed,  "\text{push}"] \& \\
\end{tikzcd}

## Checkout
allows you to "check out" a recorded state of the project

### checkout branches
    git checkout master

switch your current branch to `master`

### checkout commits
    git checkout c1d0e5f

load the snapshot recorded in the commit

> this leaves you in a detached HEAD state



# Using Git
##
\bigtext{Using Git}

## Installing Git
### Git Itself
go to [https://git-scm.org](https://git-scm.org) and follow the instructions.

### Graphical Client
Gittyup is available for Linux, Mac and even Windows. https://murmele.github.io/Gittyup/

## Set up git
if you try to use git away you'll run into issues. we need to configure it first:

    $ git config --global user.name "Your Name"
    $ git config --global user.email your@email.ch

omitting the `--global` allows configuration on a per-repo basis

##
\bigtext{Basic Commands}

## managing your repo

    $ git status            # display current working tree status
    $ git log               # print commit history
    $ git init              # initialize new git repo in the current directory
    $ git add               # add file to staging
    $ git rm                # delete file and stage that change
    $ git restore --staged  # remove file from staging
    $ git commit [Files]    # add [Files] to staging commit staging to the repo
    $ git commit -a         # like commit but stage all changed files
    $ git commit -m [TEXT]  # use the commit message [TEXT]

## working with your history

    $ git diff              # show all unstaged changes to tracked files
    $ git diff [FILE]       # display all unstaged changes to [FILE]
    $ git diff --cached     # same as git diff but looking at staged changes
    $ git reset             # unstage all changes
    $ git reset --hard      # revert all changes since the last commit
    $ git restore [FILE]    # restore unstaged file to last commit
    $ git checkout [COMMIT] # checkout commit [COMMIT]
    $ git revert [COMMIT]   # revert the changes made in commit [COMMIT]


## Exercises

> liberal use of `git status` as well as putting `--help` after any commands is recommended

* create a new git repository
* create a new file and add it to your repo and commit
* edit the file and commit the changes
* add a second file to the repo
* edit both files and commit them seperately
* checkout a previous commit
* delete or change a file and restore it


# Branches
##
\bigtext{Branches}

## what is a branch?

\begin{tikzpicture}
\draw[line width=.5pt]
    (-1,0)node[draw] {main}
    (-1,1)node[draw] {auxiliary}
    (-1,-1)node[draw] {other}
    (0,0)node[draw, circle] (0) {}
    (1,0)node[draw, circle] (1) {}
    (2,0)node[draw, circle] (2) {}
    (3,1)node[draw, circle] (3) {}
    (3,0)node[draw, circle] (4) {}
    (4,1)node[draw, circle] (5) {}
    (4,0)node[draw, circle] (6) {}
    (5,0)node[draw, circle] (7) {}
    (4,-1)node[draw, circle] (8) {}
    (6,0)node[draw, circle] (9) {}
    (5,-1)node[draw, circle] (10) {}
    [-] (0) -- (1) -- (2) -- (4) -- (6) -- (7) -- (9)
    [-] (2) -- (3) -- (5) -- (7)
    [-] (4) -- (8) -- (10)
    ;
\end{tikzpicture}


## Branches
    $ git branch [NAME]     # create a new branch with name [NAME]
    $ git branch -d [NAME]  # delete branch [NAME]
    $ git checkout [BRANCH] # checkout branch
    $ git merge [BRANCH]    # merge [BRANCH] into the current branch
    $ git rebase [BRANCH]   # rebase current branch on top of [BRANCH]

## Merge and Rebase
### Merge
\begin{tikzpicture}
\draw
    (-1, 0)node[] {main}
    (-1, 1)node[] {auxiliary}
    ;
\draw[line width=.5pt]
    (0,0)node[draw, circle] (0) {}
    (1,0)node[draw, circle] (1) {}
    (2,0)node[draw, circle] (2) {}
    (3,1)node[draw, circle] (3) {}
    (3,0)node[draw, circle, red] (4) {}
    (4,1)node[draw, circle] (5) {}
    [-] (0) -- (1) -- (2) -- (4)
    [-] (2) -- (3) -- (5)
    ;
\draw [->, double] (5, 0.5) -- (6, 0.5);
\draw[line width=.5pt]
    (7,0)node[draw, circle] (0) {}
    (8,0)node[draw, circle] (1) {}
    (9,0)node[draw, circle] (2) {}
    (10,1)node[draw, circle] (3) {}
    (10,0)node[draw, circle] (4) {}
    (11,1)node[draw, circle] (5) {}
    (12,0)node[draw, circle, red] (7) {}
    [-] (0) -- (1) -- (2) -- (4) -- (7)
    [-] (2) -- (3) -- (5) -- (7)
    ;
\end{tikzpicture}

### Rebase

\begin{tikzpicture}
\draw
    (-1, 0)node[] {main}
    (-1, 1)node[] {auxiliary}
    ;
\draw[line width=.5pt]
    (0,0)node[draw, circle] (0) {}
    (1,0)node[draw, circle] (1) {}
    (2,0)node[draw, circle] (2) {}
    (3,1)node[draw, circle] (3) {}
    (3,0)node[draw, circle] (4) {}
    (4,1)node[draw, circle, red] (5) {}
    [-] (0) -- (1) -- (2) -- (4)
    [-] (2) -- (3) -- (5)
    ;
\draw [->, double] (5, 0.5) -- (6, 0.5);
\draw[line width=.5pt]
    (7,0)node[draw, circle] (0) {}
    (8,0)node[draw, circle] (1) {}
    (9,0)node[draw, circle] (2) {}
    (10,0)node[draw, circle] (4) {}
    (11,1)node[draw, circle] (5) {}
    (12,1)node[draw, circle, red] (6) {}
    [-] (0) -- (1) -- (2) -- (4) -- (5) -- (6)
    ;
\end{tikzpicture}

## Merge Conflicts

sometimes a merge conflict will occur. what to do?

1. Keep Calm
2. use `git status`
3. follow the instructions

git adds conflict-resolution markers

    <<<<<<< master
    ...
    =======
    ...
    >>>>>>> branch

resolve these either manually or with `git mergetool`. mark as resolved with `git add`

## Exercises
* create a second branch in your existing repo
* create another branch and delete it again
* switch between branches and make edits to both
* merge or rebase between branches

advanced:

* create a merge conflict and resolve it
* merge or rebase between branches
* create a branch of a branch and merge or rebase with the original

# GitLab
## working with a remote repository
\begin{tikzcd}[ampersand replacement=\&]
\& \fbox {\text{Server}} \ar[dr, bend left, "\text{pull}"] \ar[dl, bend left, "\text{pull}"] \\
\text{Alice} \ar[ur, bend left, "\text{push}"] \& \& \text{Bob} \ar[ul, bend left, "\text{push}"]
\end{tikzcd}

* backups
* collaboration
* working from multiple computers

## Generating an SSH key
To use a remote server we need to authenticate cryptographically.
If you already have an SSH key you can skip this part.
(use `Git Bash` on windows)

    ssh-keygen -t ed25519 -C your@email.com

now we read out the content of our new public key

    cat ~/.ssh/id_ed25519.pub

1. copy the public key to your clipboard
2. go to https://gitlab.ethz.ch and log in
3. go to `Preferences -> SSH keys -> add new key`
4. paste the public key into the `key` field

## commands

    git clone [URL]             # clone remote repository into
    git clone [URL] [DIR]       # clone remote repository into directory [DIR]
    git fetch                   # fetch information from the remote repository
    git pull                    # fetch remote information and merge changes
    git pull --rebase           # rebase local changes on remote changes
    git push                    # push commits to remote
    git remote get-url origin   # get url of the remote repository
    
    git remote set-url origin [URL] # change the url for the remote repository

## how to add a remote to an existing local repository?

we not only need to add the repo

    git remote add origin [URL] # these add a remote repository

but also set it up for use

    git push -u origin --all    # push to the new remote repository and
    git pull -u origin --tags   # set it as default to be used with pull & push


## Create a repository
Under the Gitlab homepage find the `New Project` option

## Add a README
A README file contains some explanation about your project. Similar to a small landing page.
A README can be invaluable for both others and your future self.
A README should generally support

* A description
* Setup instructions
* Usage instructions
* Contribution Guidelines
* Acknowledgements

## choose a License
Baseline:

* You have copyright to the things you write
* this gives you exclusive rights

Adding a License:

* Adding a License allows others to use your code.
* Some Licenses also allow others to share and build on your code
* There are many commonly used Licenses available as templates.

## Managing Your Project

### Permissions
Gitlab allows you to configure permsissions on a branch-by-branch basis.
find this in your project under `Settings -> Repository`.

### Issues
Gitlab, like many Git hosting providers, has a built-in issue tracker. Use judiciously.

### Merge Requests
Gitlab Supports making merge requests both between branches within the same repository or from a fork.
This allows for collaborative reviewing as well as increased security.

## Exercises
* create a new Project on Gitlab
    * basic: clone it (via ssh)
    * advanced: link your existing repo to it
* push a commit
* look at the repo in web view
* create and issue

advanced:

* create a second branch and push some changes to it
* make a merge request and merge it
* more advanced: try if you can invite one of your seat neighbors to collaborate on your repo


# Troubleshooting
##
\bigtext{Troubleshooting}
## Stash and Pop
git does not allow to checkout if it would overwrite local changes.

### solution:

    git stash       # save edits to a separate "stash"
    git stash pop   # restore the changes from the "stash"


## Amending commits
scenario: you've commited something you didn't want to or left out something. You haven't pushed yet

    git commit --amend

Allows you to add the current staging not as a separate commit but as part of the previous commit.

## Links
to work with `ssh` you'll want to use ssh links to refer to your remote repository

    git@gitlab.ethz.ch:thealternative/courses.git

when you don't need to push any changes you can also clone with https

    https://gitlab.ethz.ch/thealternative/courses.git

## Merge conflicts
Remember from earlier:

1. Keep Calm
2. use `git status`
3. follow the instructions

git adds conflict-resolution markers

    <<<<<<< master
    ...
    =======
    ...
    >>>>>>> branch

resolve these either manually or with `git mergetool`. mark as resolved with `git add`

## Exercises
* make a commit and amend it
* make some changes and stash them to checkout another branch or earlier commit
* clone a repo using https to a different location (try the repo this course is in: https://gitlab.ethz.ch/thealternative/courses.git)

# Best Practices
##
\bigtext{Best Practices}

## Commit Frequency
Commit often but not too often.
Commits should represent reasonable checkpoints in the history of the project.

## Commit messages
Commit messages should be short and descriptive.

## Bad Commit Messages
#### Too short / not descriptive

    asdf

#### Too Long / not concise enough

    Added Files concerning our upcoming migration from mySQL to mariaDB so we can manage our aggregated Datasets more efficiently..

#### Off topic

    I had Pizza for lunch and it was amazing!

## Good Commit Messages

    Add Abstract
\

    Remove the old graph rendering code
\

    fix various typos in the README
\

    fixed issue #1


## Branches

Branches are a useful tool both for experimenting with rewrites and new feature or for developing a
different version of your project.
There are multiple strategies for dealing with branches such as a trunk based approach or git flow.

## Exercises
* look at your previous commit messages in the webview. are they any good?
* consider the frequency and content of commits you've made so far. do they seem sensible?

# Advanced Topics

## .gitignore
sometimes you may want some files to never get tracked by git.
add a .gitignore file

    secret.txt          # exclude specific files
    *.aux               # wildcard excludes
    !important.aux      # negate pattern
    build               # exclude directories
    build/*.pdf         # be specific

## git-lfs

for large binary files you can use git-lfs. You may need to install this seperately.

    git lfs track [FILE]    # start tracking binary file

otherwise proceed as usual

## Link commit to issue
when creating an issue on Gitlab it will recieve and issue number.
adding the issue number to your commit message will automatically link the commit to the issue.


example:

    fix issue #1

\includegraphics[width=\textwidth]{img/tag_issue.png}

## Exercises
* mention an issue in a commit
* install and try git-lfs
* make .gitignore file

# Outlook

## Where to Learn More
* look at git help pages

        git --help              # display help for git
        git [command] --help    # diplay help page for specific command

* https://git-scm.com/book/en/v2 (the Git Book)
* http://cs.stanford.edu/~blynn/gitmagic/ (full tutorial)

## Text Based Formats that work well with Git
* .tex (\LaTeX) for documents
* .md (Markdown) for notes
* .csv (Tables) for data
* .svg (Vector Graphics) for visualisation
* .html for web content
* Any Source Code files

## Git Hosting providers
* [Github.com](https://github.com) biggest platform. run by Microsoft.
* [codeberg.org](https://codeberg.org) non-commercial platform for open-source code
* [Gitlab.com](https://gitlab.com) open-source alternative for bigger projects and enterprise
* [Gitea.com](https://gitea.com) open-source git provider for self-hosting

## IDE support
* vscodium/vscode
* vim-fugitive (vim/neovim)
* magit (emacs)
* JetBrains IDEs
* Eclipse
* lazygit (standalone but huge help)

## Useful Commands

Display a beautiful commit and branch graph

    git log --decorate --graph -all
    git log --decorate --graph --all --oneline

Selectively apply commits

    git cherry-pick

## 
\bigtext{I hope you learned a lot and thank you for coming!}
