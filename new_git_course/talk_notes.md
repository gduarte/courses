## things to do better
* don't assume too much bash knowledge
* make staging clearer
* have the people preinstall git
* warn them about vim encounters
* does git windows come with nano? -> if yes, then include step to set default editor
    * yes, git does come with nano
    * can be set as default in the installer

