%%%%%%%%%%%
% Authors %
%%%%%%%%%%%

% This install guide was produced by a bunch of students of ETH Zurich part of TheAlternative.ch
% It was first written for our LinuxDays HS'16 and if it works, we might adapt it in the future.
% A participant first fills out the questionnaire, then gets a selection of the pages
% that fit his / her setup.
%
% Authors:
% Aline Abler
% Sandro Kalbermatter
% Mickey Vänskä
% Nicole Thurnherr
% Nils Leuzinger
% Lukas Tobler
% Christoph Müller
% Jonas Rembser
% Maximilian Falkenstein


%%%%%%%%%%%
% License %
%%%%%%%%%%%

% This document is licensed under the Creative Commons BY-NC-SA 4.0 license
% http://creativecommons.org/licenses/by-nc-sa/4.0/


%%%%%%%%%%%%%%%%%%%%%%%
% Hierarchy Reasoning %
%%%%%%%%%%%%%%%%%%%%%%%

% You might think the way the documents in this folder are organized is bad.
% You might say it would be more sensible to group them by topic than by distro, boot type and whatnot.
% You might say it would be easier to find single files that way.
% You would be right. It would be easier and more intuitive.
%
% However, we organized the files in this way for one specific reason: extensibility.
%
% Say we want to add another distro to the guide. What would we do?
% We simply need to copy an existing distro's folder to a new folder, and
% then modify all the files inside accordingly.
% Then we need to link these files in here. That's it.
%
% Now imagine the files were organized by topic: first you'd need to find
% out which topics even do distinguish by distro. Then, you'd need to copy
% all the single files and keep track of which ones you still need to edit,
% because they're not in a single location. It would be an unmaintainable
% mess. So that's why :-)


%%%%%%%%%%%%%%%%%
% Document Base %
%%%%%%%%%%%%%%%%%

\documentclass[a4paper,10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[pdfborder={0 0 0}]{hyperref} % Smart framework to reference labels
\usepackage{nameref} % Better references used by hyperref implicitly - We need it though explicitly called for GetTitleStringSetup
\usepackage{parskip}
\usepackage{setspace}
\usepackage{url}
\usepackage{float} % modify and manage floats
\usepackage{hypcap}
\usepackage{multicol} % manage column counts for each page
\usepackage{enumitem} % control topsep and leftmargin for lists
\usepackage[pdftex,dvipsnames,usenames]{color} % for background color
\usepackage[pdftex]{graphicx} % import graphics/images
\usepackage[font=scriptsize,format=plain,labelfont=bf,textfont=it,justification=justified,singlelinecheck=false,skip=0.05cm,labelsep=endash]{caption} % manage captions of figures
\usepackage[a4paper,left=20mm,right=20mm,top=28mm,bottom=26mm]{geometry} % manage global document page setup
\usepackage[compact,pagestyles,extramarks]{titlesec} % Set pagestyle, header and footer
\usepackage{listings}
\usepackage{array}
\usepackage{multirow}

\usepackage{etoolbox} % Manipulate commands by pre/appending to them and patching them
\usepackage{xparse} % New way of defining document-wide commands with more flexibility
\usepackage{xspace} % Smart spacing when using \newcommand for global strings
\usepackage[most]{tcolorbox} % Another way of drawing textboxes

\usepackage{wasysym} % provides \ocircle and \Box
\usepackage{forloop} % used for \Qrating and \Qlines
\usepackage{ifthen} % used for \Qitem and \QItem

\usepackage{booktabs} % Beautiful tables

\usepackage{stackengine} % Used for drawing the graphic in \warn
\usepackage{scalerel} % Used for scaling the graphic in \warn

\usepackage{colortbl}
\usepackage{xcolor}

%%%%%%%%
% Misc %
%%%%%%%%

% Set automatic linebreaks for source code listings
\lstset{breaklines=true}

% Default label
\def\labelitemi{--}

% Default path for images
\graphicspath{{img/}}

% Default graphics extensions when not explicitly mentioned - Ordered list
\DeclareGraphicsExtensions{.png,.jpg,.pdf}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document layout & spacings %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Spacings in the document
\setlength{\columnsep}{1.5\baselineskip}
\titlespacing{\section}{0pt}{2ex plus 0.5ex minus 0.5ex}{0ex plus 0.5ex}
\titlespacing{\subsection}{0pt}{2ex plus 0.5ex minus 0.5ex}{0ex plus 0.5ex}
\titlespacing{\subsubsection}{0pt}{2ex plus 0.5ex minus 0.5ex}{0ex plus 0.5ex}
\titlespacing{\subsubsubsection}{0pt}{2ex plus 0.5ex minus 0.5ex}{0ex plus 0.5ex}
\setlength{\parskip}{1mm}
\setlength{\parsep}{0pt}

% Penalties for widows/orphans (lines at top/bottom of page ending/starting)
\widowpenalty = 500 % default: 150
\clubpenalty = 500 % default: 150

% Hyphenation penalties and settings
\hyphenpenalty = 1000
\tolerance = 2000
%\emergencystretch=10pt
\lefthyphenmin = 4
\righthyphenmin = 4

% to make sure next chapter starts on odd page
\newcommand{\cleartooddpage}{
	\clearpage
	\ifodd
		\value{page}
	\else
		\null
		\clearpage
	\fi
	\setcounter{page}{1}
}

\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}


%%%%%%%%%%%%%%%%%%%%%%%%
% Counter manipulation %
%%%%%%%%%%%%%%%%%%%%%%%%

% Allow only section numbering
\setcounter{secnumdepth}{1}


%%%%%%%%%%%%%%%%%%%
% Custom sections %
%%%%%%%%%%%%%%%%%%%

\makeatletter
\def\refaddtocounter#1#2{
	\addtocounter{#1}{#2}
	\protected@edef\@currentlabel{\csname p@#1\endcsname\csname the#1\endcsname }
}

% Section that is not visible but still behaves like a section, may have a relative offset
\DeclareDocumentCommand{\sectionunlisted}{ m O{1} }{
	\refaddtocounter{section}{#2}
	\protected@edef \@currentlabel {\csname p@section\endcsname \csname thesection\endcsname}
	\NR@gettitle{#1}
	\sectionmark{#1} % Add section mark (header)
	\addcontentsline{toc}{section}{\protect\numberline{\thesection}#1} % Add section to ToC
}

% Emit a new section with a relative offset section number
\DeclareDocumentCommand{\sectionrelnum}{ m O{-1}} {
	\refaddtocounter{section}{#2} % Increment section counter by #1
	\section{#1} % Print the section
	\addcontentsline{toc}{section}{\protect\numberline{\thesection}#1}% Add section to ToC
}

% Subsection that is not visible but still behaves like a subsection, may have a relative offset
\DeclareDocumentCommand{\subsectionunlisted}{ m O{1} }{
	\refaddtocounter{subsection}{#2}
	\protected@edef \@currentlabel {\csname p@subsection\endcsname \csname thesubsection\endcsname}
	\NR@gettitle{#1}
	\subsectionmark{#1} % Add subsection mark (header)
	\addcontentsline{toc}{subsection}{\protect\numberline{\thesubsection}#1} % Add subsection to ToC
}

% Emit a new subsection with a relative offset subsection number
\DeclareDocumentCommand{\subsectionrelnum}{ m O{-1} }{
	\refaddtocounter{subsection}{#2} % Increment section counter by #2
	\subsection{#1} % Print the subsection
	\addcontentsline{toc}{subsection}{\protect\numberline{\thesubsection}#1}% Add subsection to ToC
}

% Emit a hidden subsection that is not listed in the ToC but still sets the relative counter
\DeclareDocumentCommand{\subsectionhidden}{ m O{0} }{
	\refaddtocounter{subsection}{#2}
	\protected@edef \@currentlabel {\csname p@subsection\endcsname \csname thesubsection\endcsname}
	\NR@gettitle{#1}
	\subsectionmark{#1} % Add subsection mark (header)
}

% Call a new customized section
% Arg1: Input * if we want an empty and hidden subsection
% Arg2: Section name
% Arg3: Switch for special section types
% Arg4: Passed to the special section type
\DeclareDocumentCommand{\mysection}{ s m o o }{
	\IfNoValueTF {#3}{
		\section{#2}
	}{
		\IfValueTF {#4}{
			\csname section#3\endcsname{#2}[#4]
		}{
			\csname section#3\endcsname{#2}
		}
	}
	\IfBooleanT {#1}{
		% Emit the special empty subsection
		\mysubsection{}[hidden]
	}
}

% Call a new customized subsection
% Arg1: Input * if we want to mirror our subsection prepended with {\ --\ }
% Arg2: Subsection name
% Arg3: Switch for special subsection types
% Arg4: Passed to the special subsection type
\DeclareDocumentCommand{\mysubsection}{ s m o o }{
	\IfBooleanT {#1}{
		% We want a mirrored and hidden subsection
		\subsectionhidden{\ --\ #2}
	}
	\IfNoValueTF {#3} {
		\subsection{#2}
	}{
		\IfValueT {#4}{
			\csname subsection#3\endcsname{#2}[#4]
		}{
			\csname subsection#3\endcsname{#2}
		}
	}
	\IfBooleanT {#1}{
		% We want a mirrored and hidden subsection
		\subsectionhidden{\ --\ #2}
	}
}


%%%%%%%%%%%%%%%%%%%
% Custom commands %
%%%%%%%%%%%%%%%%%%%

% Draws a danger sign, accepts a bordercolour and size as optional arguments
\DeclareDocumentCommand{\dangersign}{ O{red} O{3ex} }{
	\renewcommand\stacktype{L}
	\scaleto{\stackon[1.3pt]{\color{#1}$\triangle$}{\tiny\bfseries !}}{#2}
}

% Monospaced shell commands
\newcommand{\shellcommand}[1]{\indent\indent\texttt{#1}}
\newcommand{\shellcommandr}[1]{\indent\indent\texttt{#1}}

% Troubleshooting box
\DeclareDocumentCommand{\troubleshoot}{ +m }{
	\begin{tcolorbox}[breakable,enhanced]
		#1
	\end{tcolorbox}
}

% Free space recommendations for Linux installs
\newcommand{\freespacerec}{40\,GB\xspace}
\newcommand{\freespacemin}{20\,GB\xspace}

% Some default shortcuts for everyone to use
\newcommand{\eg}{e.g.\xspace}
\newcommand{\ie}{i.e.\xspace}
\newcommand{\os}{operating system\xspace}
\newcommand{\Os}{Operating system\xspace}
\newcommand{\mac}{\mbox{Max\,OS\,X}\xspace}
\newcommand{\winseven}{\mbox{Windows\,7}\xspace}
\newcommand{\wineight}{\mbox{Windows\,8}\xspace}
\newcommand{\winten}{\mbox{Windows\,10}\xspace}
\newcommand{\deskenvs}{Desktop Environment\xspace}
%\newcommand{}{}

% Reference section by number and name
\newcommand{\secref}[1]{\mbox{\ref{#1}.\,\nameref{#1}}}

% Reference subsection by sectionnumber, sectionname and subsectionname
\newcommand{\subsecref}[2]{\mbox{\ref{#1}.\,\nameref{#1}\nameref{#2}}}

% Standardized info; Informs the reader of why something is done, can be ommitted
\DeclareDocumentCommand{\info}{ m }{
	\virtualbox{\emph{Info: }}[#1]
}

% Standardized note; Requires to be read and adapt the solution accordingly
\DeclareDocumentCommand{\note}{ m }{
	\virtualbox{\textbf{Note: }}[#1]
}

% Standardized warning; Give a general warning about things to happen
\DeclareDocumentCommand{\warn}{ m }{
	\virtualbox{\dangersign\textbf{\textsc{Warning: }}}[#1]
}

% Standardized caution; Pay extra attention on this.
\DeclareDocumentCommand{\caution}{ m }{
	\virtualbox{\dangersign[yellow]\textbf{\textsc{Caution: }}}[#1]
}

% Standardized important; Important notification of what can be ignored for now.
\DeclareDocumentCommand{\important}{ m }{
	\virtualbox{\textbf{\textsc{Important: }}}[#1]
}

% Standardized suggestions; Our official suggestion on a topic
\DeclareDocumentCommand{\suggestions}{ m }{
	\virtualbox{\textbf{Suggestions: }}[#1]
}

% A quick command for a nicer DE mentioning
\DeclareDocumentCommand{\de}{ m }{
	\virtualbox{\textbf{DEs: }}[#1]
}

\DeclareDocumentCommand{\virtualbox}{ m O{Fill me in!} }{
	\parbox[t]{\widthof{#1}}{#1}
	\parbox[t]{\linewidth-\widthof{#1}}{#2}
}


%%%%%%%%%%%%%%%
% Page styles %
%%%%%%%%%%%%%%%

% Required to keep access all these different extramarks
\settitlemarks*{section,subsection}

% Style applied to the page when a section is defined
\newpagestyle{installguide}{
	\setheadrule{.55pt}
	\setfoot[][][] % even pages
    {\includegraphics[width=2cm]{by-nc-sa_eu}} % odd-left
	{} % odd-centre
	{\includegraphics[width=4cm]{TheAlt-bw}} % odd-right
	\sethead[][][] % even pages
	{Install Guide -- Version 1.04} % odd-left
	%{\firstextramarks{section}\textsc{\textbf\sectiontitle}{ }\emph{\firstextramarks{subsection}\subsectiontitle}}
	{\firstextramarks{section}\textbf\sectiontitle \firstextramarks{subsection}\subsectiontitle}
	{\firstextramarks{section}\thesection{ -- }\thepage} % odd-right
}

\makeatother
\begin{document}
	\pagestyle{installguide}
	\mysection{Welcome to the Install Event!}
	\label{sec:welcome}
	\input{overview}
	\pagebreak

	\mysubsection*{My system}[unlisted]
	\label{subsec:welcome-system}
	\input{system}
	\pagebreak

	\mysubsection*{Distribution}[unlisted]
	\label{subsec:welcome-distri}
	\input{distro}
	\pagebreak

	\mysubsection*{Desktop Environment}[unlisted]
	\label{subsec:welcome-de}
	\input{de}
	\cleartooddpage

	% Preparation and Boot for Win 7
	\mysection{Preparation}
	\label{sec:prep}
	\mysubsection*{Windows 7 or earlier}[unlisted]
	\label{subsec:prep-win7}
	\input{win7/preparation}
	\mysection{Boot}[relnum]
	\label{sec:boot}
	\mysubsection*{Windows 7 or earlier}[unlisted]
	\label{subsec:boot-win7}
	\input{win7/boot}
	\cleartooddpage

	% Preparation and Boot for Win 8
	\mysection{Preparation}[relnum]
	\mysubsection*{Windows 8 or later}[unlisted]
	\label{subsec:prep-win8}
	\input{win8-10/preparation}
	\mysection{Boot}[relnum]
	\mysubsection*{Windows 8 or later}[unlisted]
	\label{subsec:boot-win8}
	\input{win8-10/boot}
	\cleartooddpage

	% Preparation and Boot for Mac OS X
	\mysection{Preparation}[relnum]
	\mysubsection*{Mac OS X}[unlisted]
	\label{subsec:prep-mac}
	\input{mac/preparation}
	\mysection{Boot}[relnum]
	\mysubsection*{Mac OS X}[unlisted]
	\label{subsec:boot-mac}
	\input{mac/boot}
	\cleartooddpage

	% Installation and Partitioning for OpenSUSE
	\mysection{Installer overview}
	\label{sec:inst}
	\mysubsection*{OpenSUSE}[unlisted]
	\label{subsec:inst-suse}
	\input{suse/overview}
	\cleartooddpage
	\mysection{Partitioning}
	\label{sec:part}
	\mysubsection*{OpenSUSE}[unlisted]
	\label{subsec:part-suse}
	\input{suse/partitioning}
	\cleartooddpage

	% Installation and Partitioning for *buntu
	\mysection{Installer overview}[relnum][-2]
	\mysubsection*{*buntu}[unlisted]
	\label{subsec:inst-buntu}
	\input{buntu/overview}
	\cleartooddpage
	\mysection{Partitioning}
	\mysubsection*{*buntu}[unlisted]
	\label{subsec:part-buntu}
	\input{buntu/partitioning}
	\cleartooddpage

	% Installation and Partitioning for Fedora
	\mysection{Installer overview}[relnum][-2]
	\mysubsection*{Fedora}[unlisted]
	\label{subsec:inst-fedora}
	\input{fedora/overview}
	\cleartooddpage
	\mysection{Partitioning}
	\mysubsection*{Fedora}[unlisted]
	\label{subsec:part-fedora}
	\input{fedora/partitioning}
	\cleartooddpage

	% Partitioning Plan for EFI and Legacy
	\mysection*{Partitioning Plan}
	\label{sec:partplan}
	\input{partitionplan}
	\cleartooddpage

	\mysection*{Testing}
	\label{sec:testing}
	\input{testing}
	\cleartooddpage

	% Distro Configuration for OpenSUSE, *buntu, Fedora
	\mysection{Distribution Configuration}
	\label{sec:distconf}
	\mysubsection*{OpenSUSE}[unlisted]
	\label{subsec:distconf-suse}
	\input{suse/config}
	\cleartooddpage

	\mysection{Distribution Configuration}[relnum]
	\mysubsection*{*buntu}[unlisted]
	\label{subsec:distconf-buntu}
	\input{buntu/config}
	\cleartooddpage

	\mysection{Distribution Configuration}[relnum]
	\mysubsection*{Fedora}[unlisted]
	\label{subsec:distconf-fedora}
	\input{fedora/config}
	\cleartooddpage
	

	\mysection{Desktop Environment Configuration}
	\label{sec:deconf}
	\mysubsection*{GNOME}[unlisted]
	\label{subsec:deconf-gnome}
	\input{gnome/config}
	\cleartooddpage
	\mysection{Desktop Environment Configuration}[relnum]
	\mysubsection*{KDE}[unlisted]
	\label{subsec:deconf-kde}
	\input{kde/config}
	\cleartooddpage
	\mysection{Desktop Environment Configuration}[relnum]
	\mysubsection*{Unity}[unlisted]
	\label{subsec:deconf-unity}
	\input{unity/config}
	\cleartooddpage
	\mysection{Desktop Environment Configuration}[relnum]
	\mysubsection*{XFCE}[unlisted]
	\label{subsec:deconf-xfce}
	\input{xfce/config}
	\cleartooddpage

    \mysection*{The End}
    \input{end}
    \pagebreak
	\mysection*{Software Recommendations}[unlisted]
	\label{sec:software}
	\input{software}
\end{document}
