#!/bin/bash

# print a short help text
printhelp() {
    echo "This script separates the TheAlternative Install Guide into its subchapters to simplify printing."
    echo "It is designed to be used within the Install Guide's git directory."
    echo "The PDF is separated based on its section titles, so if these change, this script needs to be modified."
    echo "Make sure the installguide.pdf is generated and up to date."
    echo "Usage: build.sh [-vh]"
    echo "-v: verbose mode, print all the warnings from ghostscript"
    echo "-h: print this help and exit"
}

extract() {
# Extracts consecutive pages from installguide.pdf where the first page is the first one containing begin-regexp
# and the last page is the last one containing end-regexp. Output is saved to parts/output-filename.pdf
# 
# usage: extract begin-regexp end-regexp output-filename
#        extract begin-and-end-regexp output-filename

    begin_str="$1"

    # assign end_str and output based on the number of arguments
    if [[ $# -eq 2 ]]; then
        end_str="$1"
        output="$2"
    fi
    if [[ $# -eq 3 ]]; then
        end_str="$2"
        output="$( echo $3 | sed -e 's/ /_/g' )"
    fi

    echo " ---- Extracting $output.pdf ..."

    # find the page numbers of the first appearance of begin_str and the last appearance of end_str
    begin="$( pdfgrep -n -C 1 "$begin_str" installguide.pdf | head -n 1 | grep -Eo '^[0-9]*' )"
    end="$( pdfgrep -n -C 1 "$end_str" installguide.pdf | tail -n 1 | grep -Eo '^[0-9]*' )"

    # command used to extract page range from PDF
	gscommand="gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER \
	   -dFirstPage=$begin \
	   -dLastPage=$end \
	   -sOutputFile=parts/$output.pdf \
	   installguide.pdf" 

    if [[ "$verbose" == "true" ]]; then
        $gscommand
    else # If not verbose, discard warnings. (There always appear tons of warnings because invalid hyperlinks are discarded, which is fine but annoying)
        $gscommand 2>/dev/null
    fi

}

# parse options
verbose="false"
while getopts ":hv" opt; do
    case $opt in
        h)
            printhelp
            exit 1
            ;;
        v)
            verbose="true"
            ;;
        \?)
            printhelp
            exit 1
            ;;
    esac
done

# check for dependencies
if ! pdfgrep -V; then
    echo "This script requires pdfgrep. Please install pdfgrep and try again."
    exit 2
fi

if ! gs -v; then
    echo "This script requires ghostscript. Please install ghostscript and try again."
    exit 2
fi

pdflatex installguide.tex
echo
echo

mkdir -p parts


# extract all the needed parts

# If you need to add a section to this ugly hard coded mess:
# - find a regexp such that the first matching page is the first page of your section
# - find another regexp such that the last matching page is the last page of your section
# - add another extract statement below. First argument is begin-regexp, second argument is end-regexp, 
#   third argument is name of output file. If both regexp are the same, the second argument can be omitted.
#
# You can test your regexp with pdfgrep -n

extract 'Welcome to the Install Event!' 'Welcome to the Install Event! – Desktop Environment' '1_introduction'
extract 'Preparation.{0,20}Windows 7' 'Boot – Windows 7' '2_prep_win7'
extract 'Preparation.{0,20}Windows 8' 'Boot – Windows 8' '2_prep_win8-10'
extract 'Preparation – Mac OS X' 'Boot – Mac OS X' '2_prep_mac'

# This hardcoding sucks D (big time) please fix this in the future !!
extract 'Installer overview – OpenSUSE' 'Version {0,100}.{0,100}Partitioning – OpenSUSE' '3_install_partition_suse'
extract 'Installer overview – \*buntu' 'Version {0,100}.{0,100}Partitioning – \*buntu' '3_install_partition_buntu'
extract 'Installer overview – Fedora' 'Version {0,100}.{0,100}Partitioning – Fedora' '3_install_partition_fedora'
extract 'Version {0,100}.{0,100}Partitioning Plan' 'Partitioning Plan' '4_partitionplan'
extract 'Version {0,100}.{0,100}Testing' '5_testing'
extract 'Distribution Configuration – OpenSUSE' '6_distroconfig_suse'
extract 'Distribution Configuration – \*buntu' '6_distroconfig_buntu'
extract 'Distribution Configuration – Fedora' '6_distroconfig_fedora'
extract 'Desktop Environment Configuration – GNOME' '7_deconfig_gnome'
extract 'Desktop Environment Configuration – KDE' '7_deconfig_kde'
extract 'Desktop Environment Configuration – XFCE' '7_deconfig_xfce'
extract 'Desktop Environment Configuration – Unity' '7_deconfig_unity'
extract 'The End' 'Software Recommendations' '8_software'




