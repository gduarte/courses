Repair Bootcamp after repartitioning on older Macs (the ones with BIOS emulation) {#sec:Fix Bootcamp}
=================================================================================

Disk Utility will delete Hybrid MBR, so we need to fix it *(tested on
Macbook Pro 13-inch, Early 2011)*

*replace `diskX` with your disk, e.g. `disk0`*

-   `sudo fdisk /dev/diskX` to see MBR

-   `sudo gpt show/dev/diskX` to see GPT

-   Install GPT fdisk from https://sourceforge.net/projects/gptfdisk/
    (or use gdisk on linux)

-   **ALWAYS BACKUP** current Partition table to an **external
    location** first:

-   `sudo gdisk /dev/diskX` , `p` , `b`, enter backup file

-   Create Hybrid MBR:

    -   `sudo gdisk /dev/diskX` , `x`, `r`\

    -   `p`, remember partition number of bootcamp/windows

    -   `h`, enter Bootcamp partition number\

    -   Place EFI GPT (0xEE) partition first in MBR (good for GRUB)?
        (Y/N): `Y`\

    -   Enter an MBR hex code [\[]{}For bootcamp partition[\]]{}: `07`\

    -   Set the bootableflag? (Y/N): `Y`

    -   Unused partition space(s) found. Use one to protect more
        partitions? (Y/N): `N`\

    -   `p`, `o`, check if MBR is correct

    -   `w` to write hybrid MBR to disk

-   Restore GPT if something goes wrong:

    -   `sudo gdisk /dev/diskX`\

    -   `x`, `r`, `l`, enter backup file, `w`

-   If MacOS does not let you modify the MBR:

    -   reboot into recovery, open terminal, `csrutil disable`, reboot

    -   `csrutil enable` it after you are done

Install refind
--------------

*(tested on Macbook Pro 13-inch, Early 2011)*

> Bei [\[]{}Install Procedure; 1. Mac OS X[\]]{} schlägt die
> Installation von refind unter der neusten Version von Mac OS X (El
> Capitan, 10.11) fehl Grund dafür ist, dass Apple System Integrity
> Protection (‘rootless’) aktiviert hat. Genaueres dazu hier:
> http://www.rodsbooks.com/refind/sip.html Wenn man die Fehlermeldung
> (ALERT: SIP ENABLED) ignoriert und trotzdem installiert wird refind
> auf die EFI Partition kopiert, das MacBook kann davon aber nicht
> starten. Grund ist, dass der Installer von refind im gebooteten OS
> keine Rechte hat die efi boot Variablen im nvram zu ändern und der
> standard Booteintrag deshalb nicht geändert werden kann

#### Workaround 1: (würd ich vorschlagen) {#workaround-1-wuxfcrd-ich-vorschlagen}

-   siehe:
    http://www.rodsbooks.com/refind/installing.html\#manual\_renaming

-   SIP error bei refind Installation ignorieren, attempt installation
    `Y`

-   im Terminal: (evtl. mit sudo falls kein Adminaccount, verschieben
    und umbenennen geht auch von Hand im Finder) - `diskutil list` ( EFI
    partition finden, normalerweise disk0s1) -
    `diskutil mount /dev/disk0s1` - `cd /Volumes/EFI/EFI` -
    `mv refind BOOT` - `cd BOOT` - `mv refind_x64.efi bootx64.efi`

-   neu starten, Option Key (alt) gedrückt halten, und EFI BOOT
    auswählen für refind

> Vorteil: mac erkennt refind auf der EFI system partition automatisch,
> überlebt auch nvram reset, kein reboot in Recovery nötig damit es
> funktioniert

> möglicher Nachteil: mac bootet nicht automatisch von refind, sondern
> Mac OS X

> Lösung:

-   in recovery booten (Option+R), Utilities, terminal aufmachen,

    -   `diskutil mount /dev/disk0s1`

    -   `bless --mount /Volumes/EFI --setBoot --file /Volumes/EFI/EFI/BOOT/bootx64.efi --shortform`

> jetzt sollte refind automatisch booten

-   Falls keine recovery partition vorhanden geht auch bootable
    Installer (https://support.apple.com/en-us/HT201372)

#### Workaround 2: (wie von refind vorgeschlagen)

siehe: http://www.rodsbooks.com/refind/sip.html\#disable basically:

-   in recovery booten

-   refind von dort installieren

-   oder `csrutil disable`, reboot in osx, refind installieren, reboot
    in recovery, `csrutil enable`

> auf neueren MacBooks (mein 2011er hat keinen csr-active-config Eintrag
> im nvram , `nvram -p` um zu ckecken) ist es auch möglich sip mit
> refind aus- und wieder einzuschalten, dazu die EFI system Partition
> mounten, **Workaround 1**, und dann und wie hier
> http://www.rodsbooks.com/refind/sip.html\#refind\_manage beschrieben
> refind.conf editieren oder aber das wie beschrieben per stick oä
> machen

