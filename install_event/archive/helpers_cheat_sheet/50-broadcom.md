Fixing Broadcom Wireless
========================

If Wireless is not working properly under Ubuntu or openSUSE it might be
a firmware problem:

-   Under openSUSE:\
    \#sudo zypper install b43-fwcutter\
    \#sudo /usr/sbin/install\_bcm43xx\_firmware

-   Under Ubuntu:\
    \# sudo apt install firmware-b43-installer

For further reference, see:\
http://linuxwireless.org/en/users/Drivers/b43/
