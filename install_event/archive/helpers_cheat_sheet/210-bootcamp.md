\mysubsection*{Repair Bootcamp After Repartition On BIOS Macs}
\label{subsec:mac-bios}

*(tested on Macbook Pro 13-inch, Early 2011)*

`Disk Utility` will delete Hybrid MBR, so we need to fix it.

*Replace `diskX` with your disk, e.g. `disk0`*

- `sudo fdisk /dev/diskX` to see MBR
- `sudo gpt show /dev/diskX` to see GPT
- Install GPT fdisk from [https://sourceforge.net/projects/gptfdisk/](https://sourceforge.net/projects/gptfdisk/)
  (or use gdisk on Linux)
- **ALWAYS BACKUP** current partition table to an **external location** first:
    - `sudo gdisk /dev/diskX` , `p` , `b`, enter backup file
- Create Hybrid MBR:
    - `sudo gdisk /dev/diskX` , `x`, `r`
    - `p`, remember partition number of Bootcamp/Windows
    - `h`, enter Bootcamp partition number
    - Place EFI GPT (0xEE) partition first in MBR (good for GRUB)?
    (Y/N): `Y`
    - Enter an MBR hex code [\[]{}For bootcamp partition[\]]{}: `07`
    - Set the bootable flag? (Y/N): `Y`
    - Unused partition space(s) found. Use one to protect more partitions?
    (Y/N): `N`
    - `p`, `o`, check if MBR is correct
    - `w` to write hybrid MBR to disk
- In case something goes wrong: Restore GPT
    - `sudo gdisk /dev/diskX`
    - `x`, `r`, `l`, enter backup file, `w`
- If Mac OS X does not let you modify the MBR:
    - Reboot recovery, open Terminal, `csrutil disable`, reboot
    - `csrutil enable` after done

