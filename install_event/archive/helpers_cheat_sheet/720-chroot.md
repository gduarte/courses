\mysubsection*{Chroot}
\label{subsec:tricks-chroot}

To chroot into an installed system from a live iso, do the following:

* `sudo su`
* `mount /dev/sdaX /mnt`, where `sdaX` is your root partition
* If you have EFI: `mount /dev/sda1 /mnt/boot/efi` (assuming `sda1` is your EFI partition)
* `mount -t proc proc /mnt/proc/`
* `mount -t sysfs sys /mnt/sys/`
* `mount -o bind /dev /mnt/dev/`
* `chroot /mnt /bin/bash`

You're in the installed system now, as root. Congrats.

\cleartooddpage
