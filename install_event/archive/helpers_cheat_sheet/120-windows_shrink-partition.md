\mysubsection*{Shrinking Partitions}
\label{subsec:win-sparts}

Sometimes it happens that windows doesn't allow resizing a partition for some
reasons. Here is a list of helpful hints to try and make Windows comply again.
 
* Defragment Disk.
* Disable Hibernation. Restart.
    * [Win + x] -> `Command Prompt (Admin)`
    * `powercfg /h off`
* Disable Pagefile. Restart.
    * [Win + R] -> `SystemPropertiesPerformance.exe` -> Pick `Advanced`
    * In `Virtual Memory` pick `Change` and unchange `Automatically manage `
      `paging file size for all drives`
    * Set `No paging file` on partition you want to remove it from
* Disable System Protection. Restart.
    * [Win + R] -> `SystemPropertiesProtection.exe`
    * Select partition to remove protection from and click `Configure`
    * `Disable system protection` -> `Apply`
* If none of the above help, there are tools specifically made to move files around, such as EaseUS. Ask Florian Moser for a license of the tool. If you don't know who that is, ask around.

For more help: [https://technet.microsoft.com/en-us/library/cc731894.aspx](https://technet.microsoft.com/en-us/library/cc731894.aspx)

\cleartooddpage
