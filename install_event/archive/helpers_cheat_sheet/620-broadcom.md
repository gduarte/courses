\mysubsection*{Broadcom Problems}
\label{subsec:sflake-bcom}

In general, it's a good idea to look at what broadcom card you have, so you know what drivers you need:
`lspci -nn -d 14e4:`

### Broadcom Under openSUSE

Most likely, installing the broadcom-wl drivers will fix it:

-   Add packman repo: YaST → Software repos → Add, select community
    repos → Next → select packman → finish

-   `zypper ref`

-   `zypper install broadcom-wl`


### Broadcom Under Ubuntu

For reference, use: [https://help.ubuntu.com/community/WifiDocs/Driver/bcm43xx](https://help.ubuntu.com/community/WifiDocs/Driver/bcm43xx)



\cleartooddpage
