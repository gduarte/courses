\mysubsection*{Bumblebee}
\label{subsec:sflake-bbee}

Devices with new NVIDIA graphics chips often don't work right out of the box.
The best option is to use Bumblebee. `bbswitch` is used to power off
the NVIDIA chip at startup, this greatly improves battery life.

Bumblebee often doesn't work without fiddling with configuration.
(See troubleshooting section)

### Ubuntu Setup

If in doubt, check the Ubuntu wiki.

-   `sudo apt-get install bumblebee bumblebee-nvidia primus linux-headers-generic primus primus-libs-ia32`
-   `bbswitch` should be installed and enabled by default.
-   If setting up a specific driver from some repository, make sure the right
    driver is set up with Bumblebeee. See `/etc/bumblebee/bumblebee.conf`
    (Read the comments in the file)
-   Reboot

### OpenSUSE Setup

If in doubt, check the OpenSUSE wiki.

-   `sudo zypper in bumblebee bbswitch Mesa-demos-x`
-   Add user to 'bumblebee' group. `sudo usermod -aG bumblebee $username`
-   Start/enable bumblebee daemon. `sudo systemctl enable --now bumblebeed`
-   Blacklist nouveau. `echo "blacklist nouveau" | sudo tee -a /etc/modprobe.d/99-local.conf`

    Regenerate initramfs. `sudo mkinitrd`
-   If you want 32-bit applications (e.g. Steam, Games) to work:

    `zypper in Mesa-libGL1-32bit libX11-6-32bit primus-32bit`
-   Install the proprietary NVIDIA driver:

        zypper addrepo --refresh http://http.download.nvidia.com/opensuse/leap/<version> NVIDIA
        zypper install-new-recommends

    Make sure all lines in `/etc/ld.so.conf.d/nvidia-gfxG*.conf` are commented
    out.  If not, make it look like this:

        #/usr/X11R6/lib64
        #/usr/X11R6/lib

    and run `sudo ldconfig`.

    Blacklist the nvidia module:

        echo "blacklist nvidia" | sudo tee -a /etc/modprobe.d/99-local.conf
        sudo mkinitrd

    Setup xorg module directory:

        sudo mkdir -p /usr/lib64/nvidia/xorg/modules/extensions
        sudo ln -s /usr/lib64/xorg/modules/extensions/nvidia/nvidia-libglx.so /usr/lib64/nvidia/xorg/modules/extensions/libglx.so

    Configure bumblebee: Set the following in `/etc/bumblebee/bumblebee.conf`:

        [bumblebeed]
        TurnCardOffAtExit=true
        Driver=nvidia

        [driver-nvidia]
        LibraryPath=/usr/X11R6/lib64:/usr/X11R6/lib
        XorgModulePath=/usr/lib64/nvidia/xorg/modules,/usr/lib64/xorg/modules

-   Reboot

### Troubleshooting

Check the distro wikis first, but keep in mind they don't have a lot of 
troubleshooting information. Other sources:

-   Arch Wiki: https://wiki.archlinux.org/index.php/Bumblebee#Troubleshooting
-   FAQ of the Bumblebee project on Github:
    
    https://github.com/Bumblebee-Project/Bumblebee/wiki

Starting the proprietary NVIDIA settings programm:

`optirun -b none nvidia-settings -c :8`

Common problems:

-   `Bumblebee daemon is not running`

    Start the Bumblebee daemon. `systemctl enable --now bumblebeed.service`
    (If there is no systemd, use the appropriate init system)

-   `No permission to communicate with the Bumblebee daemon`

    Add the user to the 'bumblebee' group: 

    `gpasswd -a $user bumblebee`
    And log the user out and in again.

-   If you see flickering or bad performance (under a compositing WM),
    synchronize primus' display thread with the application's rendering thread:
    `PRIMUS_SYNC=1 primusrun ...`

-   Mouse lag in games: disable vsync. See 'Running applications'.
-   Capped to 60 FPS in games: disable vsync. See 'Running applications'.

### Running Applications

Applications can be run on the NVIDIA graphics card by prefixing the command
with `optirun $command` or `primusrun $command`  (if available, possibly gives
better framerate sometimes). Note that `optirun $command` doesn't use vsync by
default, but `primusrun $command` does. It can be disabled by using
`vblank_mode=0 primusrun $command`.

Launching games from Steam on the NVIDIA graphics card: Go to 'Library', then
right click on a game and click 'Properties'. Click on 'Set launch options'
and use a command like `vblank_mode=0 primusrun %command%` (Steam substitues
%command% for the game).

### Testing The Setup

Check if the graphics card is on:

-   `cat /proc/acpi/bbswitch`

Find out if bumblebee setup works correctly:

-   Run `optirun glxgears` resp. `primusrun glxgears`

(If errors occur, see Troubleshooting section.)

Find out what graphics driver is currently running the desktop:

-   `glxinfo` (look for vendor string)
-   On GNOME/similiar: Can also use the "Details" GUI tool 

\cleartooddpage
