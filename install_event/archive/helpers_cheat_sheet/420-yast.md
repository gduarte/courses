\mysubsection*{How To `YaST`}
\label{subsec:suse-yast}

- Open `YaST` via `Start Menu` -> `Settings` -> `YaST`
- Open `Software Management`
- There you can search for keywords and select packages to install

\cleartooddpage
