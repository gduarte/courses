#! /bin/sh
pandoc [0-9][0-9][0-9]-*.md --include-in-header=headers.latex --include-before-body=before_body.latex --listings --metadata=paragraph:"0" --metadata=subparagraph:"0" --variable=papersize:"a4paper" --number-section --toc --toc-depth=4 -o cheatsheet.pdf
