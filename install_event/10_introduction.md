# Introduction

This is a concise guide to install Linux. It can be used as a
standalone guide, although we recommend that you attend our Install Events
during the LinuxDays so that experienced users can help you with your
installation.

The guide help you to create an install stick, prepare your existing operating system
and finally install Linux. We also included some links to distribution-specific 
installation resources, for the most up-to-date instructions.

At the end of the guide, we include some software recommendations.

## Overview

Installing Linux is done in three steps:

- Flash an USB Stick with the Linux installer
- Prepare your existing operating system
- Install Linux

We will help you at each step. If you encounter problems or have suggestions how to improve this guide feel free to reach out!

## LinuxDays Install Event

_You can skip this part if you do not physically attend the install events_

Welcome to the LinuxDays Install Event! We are happy you could make it.

### Staff

Our staff is easily recognizable by their blue T-Shirts. Feel free to ask them
about Linux, TheAlternative, Free Software or anything else.

We will help you if you are unable to proceed with the install guide or have
any other questions. If we do not have an immediate answer, we know who to ask
or where to look it up.

Also, feel free to stay and just chat :)

### Demolaptops

We have prepared some laptops with different distribution and desktop
environments. You can try them out, and decide what you like best.

If you do not really care or can not decide, we will try to recommend
something based on what you want to use Linux for.

### USB Install Sticks & Install Guide Copies

We have prepared USB Install Sticks so you can get started right away with 
installing Linux.

Further, we have printed copies of the Install Guide if you rather have a 
printed version than look at it on you phone. 

### Anything else?

We have adapters (like USB to LAN), hubs, mouse and keyboards, ...
