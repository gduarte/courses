# Software

These are software recommendations for Linux. Most of these are graphical
programs, but there are some programs that run in the terminal only. Note that
these programs have various licenses, some might not even be "libre" software
(due to them becoming closed source at a later point, or this list having
errors)!

Some programs are annotated if they are best used on GNOME or KDE. This does
not mean they cannot be used on the other environment, but the visual
integration might be lacking in these cases.

## Office

| Use case                    | Application                            | Comment                                                                                                   |
| :---                        | :---                                   | :---                                                                                                      |
| Office suite                | LibreOffice                            |                                                                                                           |
| Office suite                | OnlyOffice                             | Looks much nicer than LibreOffice                                                                                                           |
| PDF viewer                  | Zathura                                | All desktop environments include a PDF viewer. Zathura is another, more simple option.                    |
| Note taking, PDF annotation | Xournal                                |                                                                                                           |
| LaTeX editor                | Texstudio, Texmaker, GNOME LaTeX       | Texstudio/Texmaker are "what you see is what you get", GNOME LaTeX uses a "write, then compile" workflow. |
| Mindmapping                 | vym                                    |                                                                                                           |
| Design/Publishing           | Scribus                                |                                                                                                           |
| Simple Text Editor          | Gedit/Geany (GNOME), Kwrite/Kate (KDE) |                                                                                                           |
| Advanced Text Editor        | vim, emacs                             | These are a bit more difficult to learn, but are very popular with power users.                           |
| File manager                | PCManFM, ranger/nnn (Terminal)         | All desktop environments include a file manager, these are other more advanced options.                   |
| CAD                         | FreeCAD                                |                                                                                                           |

## Graphics

| Use case               | Application                        | Comment                                                                                     |
| :---                   | :---                               | :---                                                                                        |
| Image editing          | GIMP, Pinta                        | GIMP is a more fully featured editor, similar to Photoshop. Pinta is inspired by Paint.NET. |
| Painting               | Krita                              |                                                                                             |
| 3D Animation           | Blender                            |                                                                                             |
| Photo management       | Darktable, Digikam                 |                                                                                             |
| Scanning               | Skanlite (KDE), SimpleScan (GNOME) |                                                                                             |
| OCR (Text Recognition) | OCRGui, OCRmyPDF (Terminal)        |                                                                                             |

## Multimedia

| Use case                        | Application               | Comment                                                                                  |
| :---                            | :---                      | :---                                                                                     |
| Video Player                    | mpv, VLC, Totem (GNOME)   | Desktop environments have a default video player, but mpv/VLC are usually more powerful. |
| Audio editing                   | Audacity                  |                                                                                          |
| MP3 tagging                     | Picard                    |                                                                                          |
| Digital Audio Workstation (DAW) | Ardour                    |                                                                                          |
| YouTube downloader              | youtube-dl (Terminal)     | Requires regular updates, because YouTube changes their website rather often             |
| Screen recording                | OBS, SimpleScreenRecorder | OBS is very powerful, SimpleScreenRecorder is more simple option.                        |

## Scientific

| Use case   | Application          | Comment                                                                       |
| :---       | :---                 | :---                                                                          |
| Matlab     | Python+Numpy, Octave | Octave tries to be similar to Matlab, while Python+Numpy is rather different. |
| Statistics | R, PSPP              |                                                                               |
| ArcGIS     | QGIS, GRASS GIS      |                                                                               |

## Internet

| Use case             | Application                                                       | Comment                                                       |
| :---                 | :---                                                              | :---                                                          |
| Web Browser          | Firefox, Chromium, qutebrowser                                    | We recommend using Chromium instead of Google Chrome on Linux |
| Mail                 | Thunderbird, KMail (KDE), Evolution (GNOME), mutt (Terminal)      |                                                               |
| Instant Messaging    | Telegram, Pidgin, Empathy (GNOME)                                 |                                                               |
| Torrent client       | Transmission (GNOME), Ktorrent (KDE), Deluge, rtorrent (Terminal) |                                                               |
| Voice chat           | Mumble, Empathy (GNOME)                                           |                                                               |
| File synchronization | Syncthing, Nextcloud, OwnCloud, Seafile                           |                                                               |

## System

| Use case       | Application                                      | Comment                                                         |
| :---           | :---                                             | :---                                                            |
| Disk usage     | Baobab (GNOME), Filelight (KDE), ncdu (Terminal) |                                                                 |
| Virtualization | virt-manager, VirtualBox, GNOME Boxes            |                                                                 |
| Backup         | borg backup, Déjà Dup                            |                                                                 |
| System monitor | htop (Terminal)                                  | The desktop environment have their own system monitor included. |

## Development

| Use case | Application                                                                       | Comment                                          |
| :---     | :---                                                                              | :---                                             |
| IDE      | IntelliJ, Eclipse, Code::Blocks, Android Studio, Geany, Qt Creator, Gnome Builder | Some of these might not be Free and Open Source! |

## Other

| Use case          | Application                     | Comment                                                        |
| :---              | :---                            | :---                                                           |
| Dropdown terminal | Guake (GNOME), Yakuake (KDE)    |                                                                |
| Clipboard manager | Glipper (GNOME), Clipman (XFCE) | The desktop environment each have their own clipboard manager. |

