# Install

You are all set to install Linux on your device.

If you have not yet done so; ensure you have backed up all your data!
In the (unlikely) case something goes terribly wrong, you can recover your data.

## Boot from Stick

You now want to boot from the USB stick (if you have not already done so, depending on your existing operating system).

This step can be tricky, as the way to accomplish this is different over different devices and brands.
Generally you need to look for something like "BIOS" or "startup device". 
You then need to choose the USB stick as the startup device.

You can enter the BIOS or change the startup device usually with the function keys (such as F1, F8, F10, F12) or with other keys like ESC or Enter.

The most common keys by brand:
- Lenovo: Enter
- HP: Esc

## Partitioning

This can either be done automatically by the Linux distributions
installer, or manually if you want more control over your setup. If you are
unsure about what setup you want, it might be best to follow the automatic setup.

![Example partitions for a system with Linux only](images/partitions.png)

![Example partitions for a dual boot system](images/partitions2.jpg)

### Automatic setup

The various installers usually offer an option to either install alongside an
existing operating system (Windows/Mac), or to use the entire disk and erase
previously installed systems. If you choose to install alongside something
else, there is usually an option to choose how much space you want to allocate
for the new system. Note that the partition for the old system will also have
to shrink by this amount.

### Manual setup

If you want a manual setup these are some partitions you might consider:

- `/boot`: This is the boot partition for EFI. This already exists and will be reused when installing your new operating system.
- `/` (root). A root partition is required for all installations. This is where
  all the operating system files live.
- `/home`: Some users like to their home directory on a different partition.
  This can make re-installation of a distribution easier. However, this is
  entirely optional.
- `/swap`: A swap partition is used as an extension for your machines memory,
  if it ever fills up. Nowadays, a lot of people omit this on personal computers.

You will also be able to choose a file system for `/` and `/home`. Almost
always the `ext4` filesystem should be used. If you know what you are doing,
you can of course also choose something else.

## Ubuntu

We recommend to follow the [official install-guide](https://ubuntu.com/tutorials/tutorial-install-ubuntu-desktop) for installing Ubuntu.

As further resources, we recommend the [Ubuntu Wiki](https://wiki.ubuntu.com/) and the [askubuntu StackExchange](https://askubuntu.com/).

## Fedora

The top-level documentation for Fedora can be found on the official [Fedora
docs website](https://docs.fedoraproject.org/en-US/docs/).

The following links are a good starting point:

- The Fedora [install guide](https://docs.fedoraproject.org/en-US/fedora/f31/install-guide/Introduction/)
- The [usage and customization guide](https://docs.fedoraproject.org/en-US/quick-docs/)
- The guide for upgrading to a new [release](https://docs.fedoraproject.org/en-US/quick-docs/dnf-system-upgrade/)

## Open SUSE

For getting started with OpenSuse we recommend going through [Unofficial
openSUSE Guide](https://www.opensuse-guide.org), since it gives a good overview
and can save you some time.

Also check out the [Start-Up Guide](https://doc.opensuse.org/documentation/leap/startup/single-html/book.opensuse.startup/index.html)
from their website and the [documentation](https://en.opensuse.org/Portal:Documentation) if you have
further questions or want to work with the official material.
