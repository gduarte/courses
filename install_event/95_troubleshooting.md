# Troubleshooting

A chapter for some advanced troubleshooting. If you need something contained 
in this chapter, better ask some advanced user for help (or one of our helpers if you are at our Install Event).

## Microsoft Surface

### Something doesn't work

Microsoft Surface devices usually require a patched kernel.
[See this project](https://github.com/linux-surface/linux-surface) to install
such a kernel.

## Wireless

### Broadcom on Ubuntu doesn't work

[See this.](https://help.ubuntu.com/community/WifiDocs/Driver/bcm43xx)

### Installing Broadcom Firmware

*openSUSE:*

`sudo zypper install b43-fwcutter`
`sudo /usr/sbin/install_bcm43xx_firmware`

[See also this site.](https://www.opensuse-guide.org/wlan.php)

*Ubuntu:*

`sudo apt install firmware-b43-installer`

[See also this site.](http://linuxwireless.org/en/users/Drivers/b43/)

## OpenSUSE

### Installing Broadcom Wireless Drivers (broadcom-wl)

-   Add the packman repo: YaST -> Software repos -> Add -> Select community -> repos -> Next -> Select packman -> Finish
-   `zypper ref`
-   `zypper install broadcom-wl`

## Graphics drivers

## System hangs at boot (NVIDIA present)

Boot with `nomodeset` kernel parameter, then either install proprietary NVIDIA
drivers or disable the NVIDIA graphics entirely.

## I want to disable the Nouveau driver

`echo 'blacklist nouveau' | tee /etc/modprobe.d/blacklist.conf`

## Acer

Some types of newer Acer notebooks need special settings for Secure Boot in order to boot an installed GNU/Linux system at all. If you installed eg. Ubuntu but it directly boots Windows without giving you a GRUB selection screen, try the following guide:
https://askubuntu.com/questions/771455/dual-boot-ubuntu-with-windows-on-acer-aspire/771749#771749

## System

### chroot into an installed system

To chroot into an installed system from a live iso, do the following:

- `sudo su`
- `mount /dev/sdaX /mnt`, where `sdaX` is your root partition
- If you have EFI: `mount /dev/sda1 /mnt/boot/efi` (assuming `sda1` is your EFI partition)
- `mount -t proc proc /mnt/proc/`
- `mount -t sysfs sys /mnt/sys/`
- `mount -o bind /dev /mnt/dev/`
- `chroot /mnt /bin/bash`

### Cleaning up boot entries

-   `efibootmgr` prints all boot entries.
-   `efibootmgr -o XXXX,YYYY,ZZZZ` sets the boot order.
-   `efibootmgr -b <number> -B` deletes an EFI boot entry.

### Drives are not found in the installer

Especially newer laptops (2018+) have options to switch between RAID and AHCI
boot mode.

If the laptop is running in RAID mode and Linux does not recognize its drives,
it will have to be switched to AHCI. Unfortunately, this also means Windows
needs to be reconfigured, otherwise it won't boot anymore.

1. Click the Start Button and type `cmd`
2. Right-click the result and select *Run as administrator*
3. Type this command and press *ENTER:* `bcdedit /set {current} safeboot minimal` (ALT: `bcdedit /set safeboot minimal`)
4. Restart the computer and enter BIOS Setup
5. Change the SATA Operation mode to AHCI from either IDE or RAID
6. Save changes and exit Setup and Windows will automatically boot to Safe Mode.
7. Right-click the Windows Start Menu once more. Choose Command Prompt (Admin).
8. Type this command and press ENTER: `bcdedit /deletevalue {current} safeboot` (ALT: bcdedit /deletevalue safeboot)
9. Reboot once more and Windows will automatically start with AHCI drivers enabled.

### 32-bit EFI is used

- No provided install image will boot.
- Windows is installed in 32-bit mode.
- Some older Macbooks have this configuration.
- To install 64-bit Linux (we never encountered an actual 32-bit CPU with this problem!),
  manually replace the bootloader with a grub standalone.
  [See the Arch Wiki.](https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface#Booting_64-bit_kernel_on_32-bit_UEFI)

### I need to turn off Bitlocker

**Do not change any BIOS/UEFI settings before disabling BitLocker!** You will
have to provide the decryption key otherwise, which the user typically has no
access to.

- Launch a command prompt with administrator rights and use `manage-bde -off
  C:`, where `C:` is the drive you want to decrypt.
- Use the command `manage-bde -status` to query the decryption status.
- You will need to wait until decryption is complete, which can take a long
  time.

### Unlock BitLocker Encrypted Devices

If you change any UEFI settings on a BitLocker encrypted device (typically
Surface devices), you will be prompted for the BitLocker key on next boot.

Since Surface devices come encrypted out of the box, the user does typically
not have that key and Windows will refuse to boot. If this happens, resetting
the UEFI settings to factory settings should fix the issue.

Alternatively, you can just enter the correct Bitlocker key. This works only if
the user has a Microsoft account linked to the device. You can get the
BitLocker key as follows:

* On another device, google for "BitLocker Recovery Key". You should find
  several Microsoft Support or FAQ pages on how to recover the key.
* Search for a link saying "To get your recovery key, go to BitLocker Recovery
  Keys" or similar. Go there.
* Ask the user to sign in using their Microsoft account. The website will then
  display their recovery key, which can be used to unlock the device.

### System will not boot under any circumstance

Some very bad firmwares just refuse to boot GRUB, however you configure it.
This "bootloader hack" can be applied in these cases.

- Try to boot the actual distro that has been installed by "using a device"
  to boot. Do so by holding shift while clicking on "reboot" in Windows.
- If that doesn't work, boot a live system and chroot into the installed
  system.
- Once booted or chrooted into the Linux system, become root (sudo su) and
  go to `/boot/efi/EFI/Microsoft/Boot` and locate a file named
  `bootmgfw.efi`. Rename it to `bootmgfw-original.efi`.
- Go to `/boot/efi/EFI/grub2` (sometimes also just `grub`) and locate the
  file `grubx64.efi`. Copy it over to `/boot/efi/EFI/Microsoft/Boot/`. If a
  file called `shimx64.efi` exists, copy that one over as well.
- Find the file `grub.cfg` in `/boot/efi/EFI/grub2` and copy it over to
  `/boot/efi/EFI/Microsoft/Boot/`.
- Go back to `/boot/efi/EFI/Microsoft/Boot/`. If `shimx64.efi` exists,
  rename it to `bootmgfw.efi`. If it does not exist, rename `grubx64.efi` to
  `bootmgfw.efi`.
- Now go to `/boot/grub/`, or `/boot/opensuse/` (the exact folder path may
  vary). Find the file `grub.cfg` and open it. Find the `menuentry` block
  for Windows (usually called "Windows Bootloader (on /dev/sdx)" or 
  similar). Copy the entire block.
- Uninstall the package `os-prober`.
- Now go to /etc/grub.d. If a file with `os-prober` in its name exists,
  delete it.
- Find the file `40-custom.cfg`. If it doesn't exist, create it. Paste the
  menuentry block you copied earlier in this file.
- In the text you just pasted, look for `bootmgfw.efi` and change it to
  `bootmgfw-original.efi`.
- Save the file.
- Run `grub-mkconfig -o /boot/grub/grub.cfg`. Make sure the file path
  matches the path where you originally found the `grub.cfg` you copied the
  menuentry from.
- Reboot and verify that grub now loads properly. Also test whether Windows
  boots!

