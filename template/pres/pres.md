---
author:
- Tux
title: Template Presentation
---

# Overview

## Table of Contents

### What will we do today?

* Theory
    * What is bash?
    * The basics
* Exercises

# What is bash?

## Shells

### Bash is a not a programming language.

* It is a *shell*
* It is an interface between you and your computer
* It allows you to access the operating system’s services (i.e.
    run programs)
* *It is not designed as a programming language*, but can be used
    as such

# The basics

## How to bash

### Look, a bash script

    # !/bin/bash

    echo 'Hello World'

    echo Bash is awesome

    # Now some fun stuff:
    sudo dnf update
    notify-send 'Update complete'
    feh --bg-fill 'pictures/fancy_wallpaper.jpg'
    youtube-dl -o 'Video.%(ext)s' 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'

### What bash can do

* Automate anything you can do from a console
* Let several separate programs work together


## Strings

### All about strings

\bigtext{Everything is a string}

### Meaning of strings

    ls my file 

* `ls`, `my` and `file` are single strings
* The first string becomes the command, all following become
    *arguments*

<!-- -->

    ls 'my file'

* Here, `my file` is just one string

### Remember!

\bigtext{Every word is a single argument unless you use quotes.}

## Commands

### All about commands

\bigtext{Everything that does something is a command}

### Example

* wrong:

        [[1==3]]

* Bash's answer:

        bash: [[1==3]]: command not found

* correct:

        [[ 1 == 3 ]]

## Expansion

### Bash doesn’t only run commands

* *Tilde expansion*\
	 `~/files` becomes `/home/alex/files`
* *Variable expansion*\
	 `$BROWSER` becomes `Firefox`
* *Arithmetic expansion*\
	 `$(( 1 + 4 ))` becomes `5`
* *Command substitution*\
	 `$( pwd )` becomes `/home/alex/scripts`
* *Pathname expansion* (or *globbing*)\
	 `files/qui*` becomes `files/quicknotes files/quiz`

### Bash doesn’t only run commands

* Expansion happens before any command is run
* Double quotes (") don't prevent expansion, but single quotes (') do.

        $ echo "$HOME" '$HOME'
        /home/alex $HOME

* Expansion happens *before word splitting*

## How to write a bash script

### How to write a bash script

* I want a script that searches youtube and downloads the first video it finds

### Splitting it up

* Search youtube
* Download video

### Google for a program that already does what you need

* `youtube-dl` can download a video from youtube

<!-- -->

    youtube-dl -o 'Video.%(ext)s' 'https://www.youtube.com/watch?v=lAIGb1lfpBw' 

### Course material
* These slides, exercise sheet and bash guide: \
    \soft{http://thealternative.ch}

<!-- -->

* \soft{Theme by} Christian Horea, [CC BY](https://creativecommons.org/licenses/by/4.0/)
* \soft{Original Presentation by} Aline Abler
