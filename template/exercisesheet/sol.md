# Master solutions 
## Parse some options

    #!/bin/bash
	while getopts 'h?abc:' opt; do
		case "$opt" in 
		h|\?)
			echo 'Available options: -h, -a, -b, -c ARGUMENT'
			exit 0
			;;
		a)
			echo 'Option a selected'
			;;
		b)
			echo 'Option b selected'
			;;
		c) 
			echo "Option f selected with argument $OPTARG"
			;;
		esac
	done

	shift $((OPTIND-1))

The shift line at the end is important! If you have more arguments (that aren't options), you can't access them otherwise.

## Web radio

	#!/bin/bash

	# tidy up first
	killall mpv

	# The & is not required but useful. Cache size is increased for lag free streaming
	mpv http://213.251.190.165:9000 -cache 1024 &

## Image resizer

	#!/bin/bash

	mkdir -p resized

	for i in *
	do
		# The > indicates that ImageMagick should only shrink larger images 
		convert "$i" -resize '100x100>' resized/"$i"
	done


## Assignment-fetcher

	#!/bin/bash

	while getopts 'h?lf:u:p:' opt; do
		case "$opt" in 
		h|\?)
			echo "usage: $0 [-l] [-f FILTER] [-u USER] [-p PASSWORD] URL"
			exit 0
			;;
		f) 
			filter="$OPTARG"
			;;
		u)
			user="$OPTARG"
			;;
		p)
			pass="$OPTARG"
			;;
		l) 
			list=true
		esac
	done

	shift $((OPTIND-1))

	url=$1

	
	# Bare url up to the first slash. 
    # The grep first finds all characters up to the first dot, and then from there on finds
    # all characters that are NOT slashes up to the first slash.
    # The sed is used to escape all slashes within the url. / becomes \/
	baseurl=$( \
		echo $url \
		| grep -oe '.*\.[^/]*/' \
		| sed -e 's/\//\\\//g' \
	)

	# URL without file name (up to the last slash). 
    # The grep just finds everything up to a slash. By default it uses greedy matching,
    # which means that it finds the longest fitting sequence.
    # The sed is used to escape all slashes within the url.
	longurl=$( \
		echo $url \
		| grep -oe '.*/' \
		| sed -e 's/\//\\\//g' \
	)

    # The last command used in the command chain below depends on an option.
	# if -l was passed, we just echo the found pdfs. Otherwise download them.
	if [ $list == true ]
	then
		# the -n option is for xargs
		lastcmd="-n 1 echo"
	else
		lastcmd="wget --user $user --password $pass"
	fi

	# First, find strings that end in .pdf
	# Then, prepend the base url to all strings that are relative urls
	# Then, apply the filter
	# Then, if an url starts with // (relative to root), prepend the longurl
	# last, download or display the urls
	curl -s $url \
		| grep -oe '[^"]*\.pdf' \
		| sed -e "s/^\//$baseurl\//" 
		| grep -e "/[^/]*$filter[^/]*\.pdf" \
		| awk "{ if ( \$0 !~ /\/\// ) { print \"$longurl\" \$0 } else {print} }" 2>/dev/null \
		| xargs $lastcmd
