\documentclass[aspectratio=169]{beamer}

% Local Imports
\usepackage{general}
\usepackage{slideStyle}
\usepackage{code}

\sisetup{detect-all}

% Global Imports
\usepackage{adjustbox}  

% List commands
\newcommand{\ipro}{\item[+]}
\newcommand{\icon}{\item[-]}
\newcommand{\ides}[1]{\item \textbf{#1}}
\newcommand{\iexa}[1]{\item \textbf{Example:} #1}

\DTLloaddb{server}{data/web_servers.csv}
\DTLloaddb{os}{data/web_os.csv}
\setlength{\DTLpieoutlinewidth}{0.4pt}

\DTLsetpiesegmentcolor{1}{green!40}
\DTLsetpiesegmentcolor{2}{green!30}
\DTLsetpiesegmentcolor{3}{green!20}
\DTLsetpiesegmentcolor{4}{red!40}
\DTLsetpiesegmentcolor{5}{red!20}

% fix from https://tex.stackexchange.com/questions/649809/issue-with-dtlpiechart-in-last-version-3-1-9b
\usepackage{xpatch}
\makeatletter
\xpatchcmd\DTLpiechart
 {\begin{scope}[shift={\@dtl@shift}]}
 {\edef\next{\noexpand\begin{scope}[shift={\@dtl@shift}]}\next}
 {}
 {\fail}
\makeatother 

\title{Introduction to Free and Open Source Software}
\author{Alexander Schoch}
\institute{TheAlternative}
\date{26. September 2023}

\begin{document}
    \begin{frame}
        \maketitle
    \end{frame}

    \section{Definition}
    \begin{frame}{What does \enquote{Free} mean?}
        \begin{block}{\SI{100}{\percent} unrelated to price:}
            \begin{itemize}
                \item German: \enquote{frei}
                \item French: \enquote{libre}
                \item Italian: \enquote{libero}
                \item Romansh: \enquote{liber}
            \end{itemize}
        \end{block}
    \end{frame}
    
    \begin{frame}[fragile]{What does \enquote{Open Source} mean?}
        \begin{ccode}
# include <iostream>

using namespace std;

int main () {
    const int N = 10; // larger N for slower learners
    for(int i = 0; i < N; i++) {
        cout << "FOSS is also great for non-programmers!" << endl;
    }
    return 0;
}
        \end{ccode}
    \end{frame}
    
    \begin{frame}[fragile]{}
        \begin{hex}
000007e0  06 60 8f e0 05 50 8f e0  05 60 46 e0 01 80 a0 e1  |.'...P...'F.....|
000007f0  02 90 a0 e1 5a ff ff eb  46 61 b0 e1 f0 87 bd 08  |....Z...Fa......|
00000800  00 40 a0 e3 01 40 84 e2  04 30 95 e4 09 20 a0 e1  |.@...@...0... ..|
00000810  08 10 a0 e1 07 00 a0 e1  33 ff 2f e1 04 00 56 e1  |........3./...V.|
00000820  f7 ff ff 1a f0 87 bd e8  14 07 01 00 08 07 01 00  |................|
00000830  1e ff 2f e1 08 40 2d e9  08 80 bd e8 01 00 02 00  |../..@-.........|
00000840  00 00 00 00 46 4f 53 53  20 69 73 20 61 6c 73 6f  |....FOSS is also|
00000850  20 67 72 65 61 74 20 66  6f 72 20 6e 6f 6e 2d 70  | great for non-p|
00000860  72 6f 67 72 61 6d 6d 65  72 73 21 00 40 9b 01 81  |rogrammers!.@...|
00000870  b0 b0 80 84 00 00 00 00  78 fd ff 7f 01 00 00 00  |........x.......|
00000880  60 fe ff 7f e8 ff ff 7f  c8 fe ff 7f 01 00 00 00  |'...............|
00000890  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
        \end{hex}
    \end{frame}
    
    \section{History}
    
    \begin{frame}{Early Computing}
        \begin{block}{\textcolor{ldorange}{\textasciitilde 1950-1960}: Public Domain Software, academic \enquote{hacking} culture}
            \begin{figure}[h!]
                \includegraphics[width=.45\linewidth]{images/Bendix.jpg}
                \caption{Bendix G-15 photo by Gah4 - CC BY-SA 4.0 via Commons}
            \end{figure}
        \end{block}
    \end{frame}
    
    \begin{frame}{Birth of the Proprietary Software Industry}
        \begin{block}{\textcolor{ldorange}{\textasciitilde 1970-1980}}
            \begin{itemize}
                \item Increasingly easy to withold control (e.g. binary-only distribution)
                \item United States vs. IBM (1969, dropped 1982): \enquote{Bundled Software is
anticompetitive}
                \item Computer Software Copyright Act (1980): \enquote{Software is patentable}
            \end{itemize}
        \end{block}
    \end{frame}
    
    \begin{frame}{Confronting the Software Industry}
        \begin{figure}[h!]
            \centering
            \includegraphics[width=.8\linewidth]{images/fsf.png}
        \end{figure}
        \begin{itemize}
            \item Promotes 4 universal freedoms: study, distribute, create, and modify computer
software.
            \item Promotes moral obligation to use free software.
            \item \enquote{Ethical} (dogmatic?)
        \end{itemize}
    \end{frame}
    
    \begin{frame}{Working with the Software Industry}
        \begin{figure}[h!]
            \centering
            \includegraphics[width=.2\linewidth]{images/osi.png}
        \end{figure}
        \begin{itemize}
            \item Promotes open-source principles.
            \item \enquote{Rebranded} the free software movement.
            \item Google, Facebook, etc. produce open-source software.
        \end{itemize}
    \end{frame}
    
    \begin{frame}{Sticking it together}
        \Huge\bfseries
        \begin{center}
            \textcolor{ldorange}{FOSS}
        \end{center}
    \end{frame}
    
    \section{Licenses}
    
    \begin{frame}{\textbf{GNU General Public License} -- based on the FSF's \enquote{four freedoms}}   
      \begin{itemize}
        \item The freedom to run the program as you wish, for any purpose
        \item The freedom to study how the program works, and change it so it does your
  computing as you wish
        \item The freedom to redistribute copies so you can help your neighbor
        \item The freedom to distribute copies of your modified versions to others
      \end{itemize}
    \end{frame}
    
    \begin{frame}{\textbf{BSD License} -- very permissive 3 clauses}
      Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met: 
      \begin{itemize}
        \item Redistributions of source code must retain the above copyright notice, this list of
conditions and the following disclaimer.
        \item Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution.
        \item Neither the name of the copyright holder nor the names of its contributors may be
used to endorse or promote products derived from this software without specific
prior written permission
      \end{itemize}
      
      \tiny \textcolor{gray}{THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.}
    \end{frame}
    
    \begin{frame}{Copyleft}
      \begin{figure}[h!]
            \centering
            \includegraphics[width=.2\linewidth]{images/cl.png}
        \end{figure}
        \begin{itemize}
            \item All derivatives must forever stay free
            \item May restrict interaction with non-free programs (e.g. library linking)
            \item \enquote{Viral Licensing}
        \end{itemize}
    \end{frame}
    
    \section{Advantages}
    \begin{frame}{Performance}
      \begin{center}
        \textcolor{ldorange}{Linus' Law:} \enquote{Given enough eyeballs, all bugs are shallow.}\footnote{Take this with a grain of salt}
      \end{center}
      
      \par\bigskip
      
      You: 
      
      \begin{itemize}
        \item experience less crashes.
        \item increase performance (especially for older hardware).
        \item get better support -- if ever it still fails.
      \end{itemize}
    \end{frame}
    
    \begin{frame}{Education}
      You can: 
      
      \begin{itemize}
        \item better manifest your intelligence and creativity.
        \item learn valuable, transferrable skills.
        \item get better software habits
        \begin{itemize}
          \item Open Formats (e.g. .odt or .svg)
          \item \LaTeX
          \item git
          \item CLI
        \end{itemize}
      \end{itemize}
    \end{frame}
    
    \begin{frame}{Why Open Formats?}
      \begin{minipage}[t]{.5\linewidth}
        \begin{block}{Adobe Illustrator (.ai)}
          \begin{itemize}
            \item binary with no definition on how it works
            \item hard or impossible to implement
            \item makes competing software intentionally incompatible
            \item might be patented
          \end{itemize}
        \end{block}
      \end{minipage}\hfill
      \begin{minipage}[t]{.5\linewidth}
        \begin{block}{Inkscape (.svg)}
          \begin{itemize}
            \item plain text\footnotemark
            \item open format specification
            \item can be implemented by anyone
          \end{itemize}
        \end{block}
      \end{minipage}\footnotetext{no format can ever be more open than plain text}
    \end{frame}
    
    \begin{frame}{Empowerment} % TODO: airdrop vs. kdeconnect
      You: 
      
      \begin{itemize}
        \item free yourself from vendor lock-in.
        \item get control over any service running on your system.
        \item innovate faster, never having to ask for permission.
        \item have more freedom, even than on the open market.
      \end{itemize}
    \end{frame}  
    
    \begin{frame}{Vendor Lock-In: Example}
      \begin{quote}
        \enquote{The Windows API is so broad, so deep, and so functional that most independent software vendors would be crazy not to use it. And it is so deeply embedded in the source code of many Windows apps that there is a huge switching cost to using a different operating system instead... It is this switching cost that has given the customers the patience to stick with Windows through all our mistakes, our buggy drivers, our high TCO (total cost of ownership), our lack of a sexy vision at times, and many other difficulties [...] Customers constantly evaluate other desktop platforms, [but] it would be so much work to move over that they hope we just improve Windows rather than force them to move. In short, without this exclusive franchise called the Windows API, we would have been dead a long time ago.}
      \end{quote}
      -- Aaron Contorer, Head of C++ development at Microsoft
    \end{frame}
    
    \begin{frame}{Vendor Lock-In: Example 2}
      \begin{minipage}{.5\linewidth}
        \begin{figure}[H]
          \centering
          \includegraphics[width=.5\linewidth]{images/airdrop.png}
        \end{figure}
      \end{minipage}\hfill
      \begin{minipage}{.5\linewidth}
        \begin{figure}[H]
          \centering
          \includegraphics[width=.5\linewidth]{images/kdeconnect.png}
        \end{figure}
      \end{minipage}
    \end{frame}
      
    \begin{frame}{Security}
      You: 
      
      \begin{itemize} % TODO: north korea and russia use linux, open source software in germany
        \item benefit from more privacy.
        \item use software which is more transparent (as for government -- i.e. trustworthy).
        \item have access to better encryption.
      \end{itemize}
    \end{frame}
    
    \begin{frame}{Sustainability} % TODO: python vs. excel -- show source code
      \begin{center}
        \enquote{the quality of being able to continue over a period of time}
      \end{center}
      
      \par\bigskip
      
      The Software:
      \begin{itemize}
        \item depends on no single entity for its continuity.
        \item can be improved by all and degraded by none.
        \item is reproducible and transparent (as for science -- i.e. comprehensible).
      \end{itemize}
    \end{frame}
    
    \begin{frame}{Sustainability: Example} % TODO: python vs. excel -- show source code
      Fundamental in scientific research:
      \begin{itemize}
        \item Transparency
        \item Reproducability
      \end{itemize}
      
      \begin{minipage}[t]{.5\linewidth}
        \begin{block}{Microsoft Excel}
          \begin{itemize}
            \item not transparent: not possible to check if the functions do what they should do
            \item not reproducible: The blackbox makes it difficult to get the same conditions
          \end{itemize}
        \end{block}
      \end{minipage}\hfill
      \begin{minipage}[t]{.5\linewidth}
        \begin{block}{Python}
          \begin{itemize}
            \item transparent: The source code has all information about any function
            \item reproducible: The same source code leads to the same results
          \end{itemize}
        \end{block}
      \end{minipage}
    \end{frame}
    
    \section{Challenges} % using other people's computer, having no control
    \begin{frame}{Software as a Service}
      \begin{minipage}{.33\linewidth}
        \begin{figure}[H]
          \centering
          \includegraphics[width=.5\linewidth]{images/drive.png}
        \end{figure}
      \end{minipage}\hfill
      \begin{minipage}{.33\linewidth}
        \begin{figure}[H]
          \centering
          \includegraphics[width=.5\linewidth]{images/dropbox.png}
        \end{figure}
      \end{minipage}\hfill
      \begin{minipage}{.33\linewidth}
        \begin{figure}[H]
          \centering
          \includegraphics[width=.5\linewidth]{images/icloud.png}
        \end{figure}
      \end{minipage}
      
      \par\bigskip
      
      \begin{itemize}
        \item Also known as SaaS or \enquote{the cloud}
        \item May be (based on) FOSS
        \item Usage paradigm is unfree, closed, and unsustainable
      \end{itemize}
      
    \end{frame}
    
  	\begin{frame}{Regulation - from without}
			E.g. via laws and regulations:
			\begin{itemize}
				\item \enquote{Privacy} threatens admissible information sources.
				\item \enquote{Copyright} threatens admissible information matter.
				\begin{itemize}
					\item Embrace, Extend, Extinguish as a business model.
				\end{itemize}
				\item \enquote{Consumer Protection} threatens admissible information destinations.
			\end{itemize}
		\end{frame}

    
    
    \begin{frame}{Embrace, Extend, Extinguish: Example}
      \begin{itemize}
        \item \textcolor{ldorange}{Embrace:} Google joining the browser market with Chrome/Chromium
        \item  \textcolor{ldorange}{Extend:} Add proprietary features that are not standard-compliant. Google apps use these features, leading to compatibility problems on other browsers.
        \item  \textcolor{ldorange}{Extinguish:} 
        \begin{itemize}
          \item Developers don't test their applications on other browsers
          \item Applications break
          \item Other browsers lose market share due to \enquote{being bad}
        \end{itemize}
      \end{itemize}
    \end{frame}
    
    \begin{frame}{Regulation from within} % TODO: more info
      E.g. \textit{via} political \enquote{Codes of Conduct}, which can:
      \begin{itemize}
        \item make participation contingent on ideology (less free).
        \item encourage exclusion from the community (less open).
        \item reduce resource pooling (less sustainable).
      \end{itemize}
    \end{frame}
    
    \section{Examples}
    \begin{frame}{Linux Kernel}
      \begin{figure}[H]
        \centering
        \includegraphics[width=.2\linewidth]{images/tux.png}
        \caption{\enquote{Tux} by Larry Ewing, Simon Budig, Anja Gerwinski Licensed under Attribution via Commons}
      \end{figure}
      As of 2010, only \SI{2}{\percent} of the Linux Kernel (\SI{3.034e7}{LOC} in 2021) was written by Linus Torvalds.
    \end{frame}
    
    \begin{frame}{The GNU/Linux Operating System}
      \begin{minipage}{.5\linewidth}
        Developed by the FSF:
        \begin{itemize}
          \item GNU Compiler Collection (gcc)
          \item GNOME
          \item GIMP, GNU Octave, GnuCash, etc.
        \end{itemize}
      \end{minipage}\hfill
      \begin{minipage}{.5\linewidth}
        \begin{figure}[H]
          \centering
          \includegraphics[width=.8\linewidth]{images/gp.png}
          \caption{\enquote{Gnu-and-penguin-color} by FSF -- CC BY-SA
3.0 via Commons}
        \end{figure}
      \end{minipage}
    \end{frame}
    
    \begin{frame}{\textbf{SO} much more}
      \begin{itemize}
        \item \textcolor{ldorange}{Desktop:} Chromium, Firefox, LibreOffice, OnlyOffice, \LaTeX, \textcolor{gray}{etc.}
        \item \textcolor{ldorange}{Multimedia:} mpv, VLC, Audacity, Kdenlive, \textcolor{gray}{etc.}
        \item \textcolor{ldorange}{Scientific:} NumPy, matplotlib, JabRef, R, GNU Octave, Jupyter \textcolor{gray}{etc.}
        \item \textcolor{ldorange}{Server/Cloud:} Openstack, WordPress, NextCloud, Mastodon, GitLab, \textcolor{gray}{etc.}
        \item \textcolor{ldorange}{Graphics:} RawTherapee, GIMP, Inkscape, Blender, Darktable, Krita, \textcolor{gray}{etc.}
        \item \textcolor{orange}{Programming:} VSCodium, Spyder, git, gcc, python, \textcolor{gray}{etc.}
        \item \textcolor{gray}{etc.}
      \end{itemize}
    \end{frame}
    
    \section{Users/Developers}
    \begin{frame}{The Internet}
      \begin{minipage}[b]{.5\linewidth}
        \begin{figure}[H]
          \centering
          \tiny
          \DTLpiechart{radius=0.235\textheight,variable=\quantity,innerlabel={\SI{\DTLpiepercent}{\percent}},outerlabel=\name,cutaway={1-3},innerratio=.7}{server}{\name=Name,\quantity=percent}
          \caption{Web Server Market Share 2022-09-27 via \href{http://w3techs.com/technologies/overview/operating_system/all}{w3techs}}
        \end{figure}
      \end{minipage}\hfill
      \begin{minipage}[b]{.5\linewidth}
        \begin{figure}[H]
          \centering
          \tiny
          	%change color palette to preserve green for free and red for non-free
	            \DTLsetpiesegmentcolor{1}{green!40}
	            \DTLsetpiesegmentcolor{2}{red!40}
							\DTLpiechart{radius=0.265\textheight,variable=\quantity,innerlabel={\SI{\DTLpiepercent}{\percent}},outerlabel=\name,cutaway={2},innerratio=.7}{os}{\name=Name,\quantity=percent}
							\vspace{.2cm}
							\caption{Website OS Market Share 2022-09-27 via \href{http://w3techs.com/technologies/overview/operating_system/all}{w3techs}}
        \end{figure}
      \end{minipage}
    \end{frame}
    
    \begin{frame}{Public Institutions}
      \begin{itemize}
        \item Governments (benefits: strategic, economic)
        \item ETH
        \begin{itemize}
          \item Linux Cluster running CentOS (Euler)
          \item Fedora/Windows dual boot on public computers
        \end{itemize}
        \item UZH
        \begin{itemize}
          \item OpenStack-based cluster (ScienceCloud)
        \end{itemize}
      \end{itemize}
    \end{frame}
    
    \begin{frame}{Businesses}
      \begin{minipage}{.5\linewidth}
        \begin{figure}[H]
          \includegraphics[width=.6\linewidth]{images/chromium.png}
          \caption{“Chromium Material Icon” by The Chromium Project - CC BY 2.5 via \href{https://commons.wikimedia.org/wiki/File:Chromium_Material_Icon-256x256.png}{Commons}}
        \end{figure}
      \end{minipage}\hfill
      \begin{minipage}{.5\linewidth}
        \begin{figure}[H]
          \includegraphics[width=.6\linewidth]{images/chrome.png}
          \caption{“Google Chrome Material Icon” by Google, Google Play. - BSD License via \href{https://commons.wikimedia.org/wiki/File:Chromium_Material_Icon-256x256.png}{Commons}}
        \end{figure}
      \end{minipage}
    \end{frame}
    
    \begin{frame}{Businesses}
      \begin{figure}[H]
        \includegraphics[width=.6\linewidth]{images/rh.png}
        \caption{\copyright Red Hat, Inc.}
      \end{figure}
      \begin{itemize}
				\item Founded 1993, subsidiary of IBM since 2019
				\item 2015--2018 stock price $\beta_\text{RHT}=1.16$ (e.g. $\beta_\text{MSFT}=1.22$)
				\item Very diverse open source products, e.g. OpenStack, RHEL, CloudForms
			\end{itemize}
    \end{frame}

    \begin{frame}{Business Models based on FOSS}
      \begin{itemize}
        \item Paid Build: Threema
        \item Paid Support: RedHat, SUSE
        \item Paid Hosting (SaaS): Vercel, WordPress, GitLab, Nextcloud
        \item Paid Products: tailwindcss, Mantine
        \item Paid Development: Ubique, Alexander Schoch
        \item Dual License: Elasticsearch, MongoDB, Qt
      \end{itemize}
    \end{frame}

    \begin{frame}{Developers}
      \begin{minipage}[t]{.5\linewidth}
        \begin{figure}[H]
          \includegraphics[width=.6\linewidth]{images/godot.pdf}
          \vspace{.76cm}
          \caption{“Godot Engine Logo” by Godot - CC BY 4.0 via \href{https://godotengine.org/press/}{Godot Website}}
        \end{figure}
      \end{minipage}\hfill
      \begin{minipage}[t]{.5\linewidth}
        \begin{figure}[H]
          \includegraphics[width=.6\linewidth]{images/unity.pdf}
          \caption{“Made with Unity Logo” by Unity Technologies - Proprietary License via \href{https://brandfolder.com/s/3v2rb9wqbr9sjbh5gqmmcq}{Brandfolder}}
        \end{figure}
      \end{minipage}
    \end{frame}

    \begin{frame}{The Fediverse}
      \begin{minipage}[t]{.5\linewidth}
        \begin{figure}[H]
          \includegraphics[width=.6\linewidth]{images/mastodon.pdf}
          \caption{“Mastodon Logo” by Mastodon - GNU AGPL 3.0 via \href{https://commons.wikimedia.org/wiki/File:Mastodon_Logotype_(Simple).svg}{Commons}}
        \end{figure}
      \end{minipage}\hfill
      \begin{minipage}[t]{.5\linewidth}
        \begin{figure}[H]
          \includegraphics[width=.6\linewidth]{images/x.pdf}
          \vspace{.14cm}
          \caption{“X Logo” by Twitter - Proprietary License via \href{https://about.twitter.com/en/who-we-are/brand-toolkit}{Twitter}}
        \end{figure}
      \end{minipage}
    \end{frame}
    
    \begin{frame}{The Persecuted}
			\begin{figure}
			\centering
				\includegraphics[width=0.55\textheight]{images/tor.png}
				\caption{“Tor-logo-2011-flat” by Tor Project - CC BY 3.0 via \href{https://commons.wikimedia.org/wiki/File:Tor-logo-2011-flat.svg}{Commons}}
			\end{figure}
			Free and Open by design, not by convention.
			\begin{multicols}{3}
				\begin{itemize}
					\item Whistleblowers
					\item Marginalized Gvmts.
					\item (Alleged) Criminals
				\end{itemize}
			\end{multicols}
		\end{frame}
		
		\begin{frame}{The Persecuted: Example} % TODO: signal slide
      \begin{minipage}{.5\linewidth}
        \begin{figure}[H]
          \includegraphics[width=.6\linewidth]{images/whatsapp.png}
          \caption{WhatsApp}
        \end{figure}
      \end{minipage}\hfill
      \begin{minipage}{.5\linewidth}
        \begin{figure}[H]
          \includegraphics[width=.6\linewidth]{images/signal.png}
          \caption{Signal}
        \end{figure}
      \end{minipage}
		\end{frame}
		
		\begin{frame}{The Busy and Creative}
			\begin{itemize}
				\item Students
				\item Scientists
				\item Engineers
				\item Analysts
				\item Artists
			\end{itemize}
			\center \textit{Get the opportunity to learn \textbf{the right way}, keep the freedom to do it \textbf{your way}.}
		\end{frame}
		\begin{frame}{Most of all: You}
			\begin{itemize}
				\item The sooner the better
				\item Less time-intensive than 1\,ETCS
				\item You have already started!
			\end{itemize}
		\end{frame}

	  \section{Meta}
		\subsection{About this presentation}
		\begin{frame}{What now?}
			\begin{itemize}
				\item Q\&A round\\
				\textcolor{gray}{in a few seconds}
				\item Come to the next LinuxDays events: \\
				\textcolor{gray}{\url{https://thealternative.ch/}. "Introdution to Linux" on Tuesday next week!}
				\item Join the next Stammtisch:\\
				\textcolor{gray}{Thursday @ SPH}
			\end{itemize}
		\end{frame}
		
		\begin{frame}{These Slides}
			\begin{itemize}
        \item Links on \url{https://thealternative.ch}
				\item \textcolor{gray}{Latest Slides:}\\ \url{https://gitlab.ethz.ch/thealternative/courses/-/blob/master/FS24_FOSS/main.pdf}
				\item \textcolor{gray}{Source:}\\ \url{https://gitlab.ethz.ch/thealternative/courses/-/tree/master/FS24_FOSS}
			\end{itemize}
			%separate sources from info so it looks nicer
			\begin{itemize}
				\item \textcolor{gray}{License:} \href{https://creativecommons.org/licenses/by-sa/4.0/}{Slides by Alexander Schoch/Christian Horea, CC BY-SA 4.0}
			\end{itemize}
		\end{frame}

    
    % EEE: 1. google embracing web standards
    %      2. google extending them by adding proprietary non-standard features
    %      3. extinguish: some features not working on other browsers. developers who use them will break their stuff on e.g. firefox
    
    % whistleblowers --> signal vs. w'app. fbi asked for user data, they had none + unix millis
\end{document}
