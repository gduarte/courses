#!/bin/bash
echo "Building exercises pdf..."
pandoc sheet.md --listings --toc -V links-as-notes -H listings-setup.tex -o sheet.pdf \
    && echo "Build successful"
echo "Building solutions pdf..."
pandoc sol.md --listings -H listings-setup.tex -o sol.pdf \
    && echo "Build successful"
