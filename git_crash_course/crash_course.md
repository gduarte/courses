---
author:
- Fadri Lardon
title: Git Crashcourse
---
 

# Introduction

## Who are we?
\includegraphics[width=\textwidth]{img/logo_blue.pdf}

## What git?
* THE modern distributed version control system
* originally written to develop Linux (the Kernel you may be using)

## Why git?
* change history with the ability to undo almost any operation
* easy to collaborate with others and yourself
* ensure file integrity
* branches

## Where git?
* github
* linux
* literally like almost every open (or closed)- source project
* your studies

## How git?
That's why you're here!

# Basic Concepts
##
\bigtext{Basic Concepts}

## Terminology
### commit
represents a snapshot of your project this is the smallest unit of change that git records

### branch
a chain of commits under a specific name

### repository
the structure in which git stores your data, effectively your project folder

## Repository

 \usetikzlibrary {arrows.meta}

 \begin{tikzpicture}
\draw[line width=.5pt]
    (4, 3) node[draw] {Repository}
    (-1,0)node[draw] {main}
    (-1,1)node[draw] {auxiliary}
    (-1,-1)node[draw] {other}
    (0,0)node[draw, circle] (0) {}
    (1,0)node[draw, circle] (1) {}
    (2,0)node[draw, circle] (2) {}
    (3,1)node[draw, circle] (3) {}
    (3,0)node[draw, circle] (4) {}
    (4,1)node[draw, circle] (5) {}
    (4,0)node[draw, circle] (6) {}
    (5,0)node[draw, circle] (7) {}
    (4,-1)node[draw, circle] (8) {}
    (6,0)node[draw, circle] (9) {}
    (5,-1)node[draw, circle] (10) {}
    [-] (0) -- (1) -- (2) -- (4) -- (6) -- (7) -- (9)
    [-] (2) -- (3) -- (5) -- (7)
    [-] (4) -- (8) -- (10)
    ;
\end{tikzpicture}

## States of Git

\begin{tikzcd}[ampersand replacement=\&]
    \text{working directory} \ar[rr, out=50, in=130, "\text{git add}"] \& \&  \text{staging area} \ar[ld, bend left, "\text{git commit}"] \\
    \& \text{repository}
    \ar[lu, bend left, "\text{git checkout}"]
\end{tikzcd}

## Git is distributed
\begin{tikzcd}[ampersand replacement=\&]
\& \fbox{Server} \ar[dr] \ar[dl] \\
\fbox{Alice} \ar[ur] \ar[rr, red]\& \& \fbox{Bob} \ar[ul] \ar[ll, red]
\end{tikzcd}


* every repo is a full copy of the project
* cooperation does not require a server

## Git Workflow

\begin{tikzcd}[ampersand replacement=\&]
    \text{local repository} \ar[d, bend right, "\text{change files}"] \ar[dr, bend left, in=160, out=20, "\text{create files}"] \\
    \text{unstaged changes} \ar[d, bend right, "\text{commit changes}"] \&  \text{untracked files} \ar[dl, bend left, out=10, in=170,  "\text{add to repo}"] \& \text{remote repository} \ar[llu, bend right, dashed,  "\text{pull}"] \\
    \text{local commits} \ar[rru, bend right, dashed,  "\text{push}"] \& \\
\end{tikzcd}


# Using Git
##
\bigtext{Using Git}

## Installing Git

go to
    https://git-scm.com
and follow instructions

## Set up git
if you try to use git right away you'll run into issues. we need to configure it first:

    $ git config --global user.name "Your Name"
    $ git config --global user.email your@email.ch
    $ git config --global core.editor nano

omitting the `--global` allows configuration on a per-repo basis

##
\bigtext{Basic Commands}

## managing your repo

    $ git status            # display current working tree status
    $ git log               # print commit history
    $ git init              # initialize new git repo in the current directory
    $ git add               # add file to staging
    $ git rm                # delete file and stage that change
    $ git restore --staged  # remove file from staging
    $ git commit <Files>    # add <Files> to staging commit staging to the repo
    $ git commit -a         # like commit but stage all changed files
    $ git commit -m <TEXT>  # use the commit message <TEXT>

## working with your history

    $ git diff              # show all unstaged changes to tracked files
    $ git diff <FILE>       # display all unstaged changes to <FILE>
    $ git apply             # apply output of git diff from stdinA
    $ git apply <PATCH>     # apply patch from file
    $ git reset             # unstage all changes
    $ git reset --hard      # revert all changes since the last commit
    $ git restore <FILE>    # restore unstaged file to last commit
    $ git checkout <COMMIT> # checkout commit <COMMIT>
    $ git revert <COMMIT>   # revert the changes made in commit <COMMIT>

## Checkout
allows you to "check out" a recorded state of the project

### checkout branches
    git checkout master

switch your current branch to `master`

### checkout commits
    git checkout c1d0e5f

load the snapshot recorded in the commit

> this leaves you in a detached HEAD state

# Branches
##
\bigtext{Branches}

## what is a branch?

\begin{tikzpicture}
\draw[line width=.5pt]
    (-1,0)node[draw] {main}
    (-1,1)node[draw] {auxiliary}
    (-1,-1)node[draw] {other}
    (0,0)node[draw, circle] (0) {}
    (1,0)node[draw, circle] (1) {}
    (2,0)node[draw, circle] (2) {}
    (3,1)node[draw, circle] (3) {}
    (3,0)node[draw, circle] (4) {}
    (4,1)node[draw, circle] (5) {}
    (4,0)node[draw, circle] (6) {}
    (5,0)node[draw, circle] (7) {}
    (4,-1)node[draw, circle] (8) {}
    (6,0)node[draw, circle] (9) {}
    (5,-1)node[draw, circle] (10) {}
    [-] (0) -- (1) -- (2) -- (4) -- (6) -- (7) -- (9)
    [-] (2) -- (3) -- (5) -- (7)
    [-] (4) -- (8) -- (10)
    ;
\end{tikzpicture}


## Branches
    $ git branch <NAME>     # create a new branch with name <NAME>
    $ git branch -d <NAME>  # delete branch <NAME>
    $ git checkout <BRANCH> # checkout branch
    $ git switch   <BRANCH> # switch to branch (same as checkout)
    $ git merge <BRANCH>    # merge <BRANCH> into the current branch
    $ git rebase <BRANCH>   # rebase current branch on top of <BRANCH>

## Merge and Rebase
### Merge
\begin{tikzpicture}
\draw
    (-1, 0)node[] {main}
    (-1, 1)node[] {auxiliary}
    ;
\draw[line width=.5pt]
    (0,0)node[draw, circle] (0) {}
    (1,0)node[draw, circle] (1) {}
    (2,0)node[draw, circle] (2) {}
    (3,1)node[draw, circle] (3) {}
    (3,0)node[draw, circle, red] (4) {}
    (4,1)node[draw, circle] (5) {}
    [-] (0) -- (1) -- (2) -- (4)
    [-] (2) -- (3) -- (5)
    ;
\draw [->, double] (5, 0.5) -- (6, 0.5);
\draw[line width=.5pt]
    (7,0)node[draw, circle] (0) {}
    (8,0)node[draw, circle] (1) {}
    (9,0)node[draw, circle] (2) {}
    (10,1)node[draw, circle] (3) {}
    (10,0)node[draw, circle] (4) {}
    (11,1)node[draw, circle] (5) {}
    (12,0)node[draw, circle, red] (7) {}
    [-] (0) -- (1) -- (2) -- (4) -- (7)
    [-] (2) -- (3) -- (5) -- (7)
    ;
\end{tikzpicture}

### Rebase

\begin{tikzpicture}
\draw
    (-1, 0)node[] {main}
    (-1, 1)node[] {auxiliary}
    ;
\draw[line width=.5pt]
    (0,0)node[draw, circle] (0) {}
    (1,0)node[draw, circle] (1) {}
    (2,0)node[draw, circle] (2) {}
    (3,1)node[draw, circle] (3) {}
    (3,0)node[draw, circle, blue] (4) {}
    (4,1)node[draw, circle, red] (5) {}
    [-] (0) -- (1) -- (2) -- (4)
    [-] (2) -- (3) -- (5)
    ;
\draw [->, double] (5, 0.5) -- (6, 0.5);
\draw[line width=.5pt]
    (7,0)node[draw, circle] (0) {}
    (8,0)node[draw, circle] (1) {}
    (9,0)node[draw, circle] (2) {}
    (10,0)node[draw, circle, blue] (3) {}
    (11,1)node[draw, circle] (4) {}
    (12,1)node[draw, circle, red] (5) {}
    [-] (0) -- (1) -- (2) -- (3) -- (4) -- (5)
    ;
\end{tikzpicture}

## Merge Conflicts

sometimes a merge conflict will occur. what to do?

1. Keep Calm
2. use `git status`
3. follow the instructions

git adds conflict-resolution markers

    <<<<<<< master
    ...
    =======
    ...
    >>>>>>> branch

resolve these manually and mark as resolved with `git add`

# Remote Repositories

## URL types 
to work with `ssh` you'll want to use ssh links to refer to your remote repository

    git@gitlab.ethz.ch:thealternative/courses.git

when you don't need to push any changes you can also clone with https

    https://gitlab.ethz.ch/thealternative/courses.git

## remote commands

    git clone <URL>             # clone remote repository into
    git clone <URL> <DIR>       # clone remote repository into directory <DIR>
    git fetch                   # fetch information from the remote repository
    git pull                    # fetch remote information and merge changes
    git pull --rebase           # rebase local changes on remote changes
    git push                    # push commits to remote
    git remote get-url origin   # get url of the remote repository
    git remote set-url origin <URL> # change the url for the remote repository

## how to add a remote to an existing local repository?

we not only need to add the repo

    git remote add origin <URL> # these add a remote repository

but also set it up for use

    git push -u origin --all    # push to the new remote repository and
    git pull -u origin --tags   # set it as default to be used with pull & push

## Managing a Project on Gitlab

### Permissions
Gitlab allows you to configure permissions on a branch-by-branch basis.
Find this in your project under `Settings -> Repository`.

### Issues
Gitlab, like many Git hosting providers, has a built-in issue tracker. Use judiciously.

### Merge Requests
Gitlab Supports making merge requests both between branches within the same repository or from a fork.
This allows for collaborative reviewing as well as increased security.

## Remember the workflow?
\begin{tikzcd}[ampersand replacement=\&]
    \text{local repository} \ar[d, bend right, "\text{change files}"] \ar[dr, bend left, in=160, out=20, "\text{create files}"] \\
    \text{unstaged changes} \ar[d, bend right, "\text{commit changes}"] \&  \text{untracked files} \ar[dl, bend left, out=10, in=170,  "\text{add to repo}"] \& \text{remote repository} \ar[llu, bend right, dashed,  "\text{pull}"] \\
    \text{local commits} \ar[rru, bend right, dashed,  "\text{push}"] \& \\
\end{tikzcd}

# Good Practices
##
\bigtext{Good Practices}

## Commit Frequency
Commit often but not too often.
Each commits should represent a reasonable checkpoint in the history of the project.

## Commit messages
Commit messages should be short and descriptive.

## Bad Commit Messages
#### Too short / not descriptive

    asdf

#### Too Long / not concise enough

    Added Files concerning our upcoming migration from mySQL to mariaDB so we can manage our aggregated Datasets more efficiently..

#### Off topic

    I had Pizza for lunch and it was amazing!

## Good Commit Messages

    Add Abstract
\

    Remove the old graph rendering code
\

    fix various typos in the README
\

    fixed issue #1

<!--## Full commit style

    Capitalized, short (50 chars or less) summary

    More detailed explanatory text, if necessary. Wrap it to about 72
    characters or so. In some contexts, the first line is treated as the
    subject of an email and the rest of the text as the body. The blank
    line separating the summary from the body is critical (unless you omit
    the body entirely); tools like rebase will confuse you if you run the
    two together.

    Write your commit message in the imperative: "Fix bug" and not "Fixed bug"
    or "Fixes bug." This convention matches up with commit messages generated
    by commands like git merge and git revert.

    Further paragraphs come after blank lines.

    - Bullet points are okay, too

    - Typically a hyphen or asterisk is used for the bullet, followed by a
    single space, with blank lines in between, but conventions vary here

    - Use a hanging indentA
-->
## Branches

Branches are a useful tool both for experimenting with rewrites and new feature or for developing a
different version of your project.
There are multiple strategies for dealing with branches such as a trunk based approach or git flow.

# Advanced Topics & useful tricks

## .gitignore
sometimes you may want some files to never get tracked by git.
add a .gitignore file

    secret.txt          # exclude specific files
    *.aux               # wildcard excludes
    !important.aux      # negate pattern
    build               # exclude directories
    build/*.pdf         # be specific

## git-lfs

for large binary files you can use git-lfs. You may need to install this seperately.

    git lfs track <FILE>    # start tracking binary file

otherwise proceed as usual

## Link commit to issue
when creating an issue on Gitlab it will recieve and issue number.
adding the issue number to your commit message will automatically link the commit to the issue.


example:

    fix issue #1

\includegraphics[width=\textwidth]{img/tag_issue.png}

## Git log 

Display a beautiful commit and branch graph

    git log --decorate --graph -all
    git log --decorate --graph --all --oneline

## Stash and Pop
git does not allow to checkout if it would overwrite local changes.

### solution:
    git stash       # save edits to a separate "stash"
    git stash pop   # restore the changes from the "stash"

### this can be messy

## Amending commits
scenario: you've commited something you didn't want to or left out something. You haven't pushed yet

    $ git commit --amend

Allows you to add the current staging not as a separate commit but as part of the previous commit.



## Merge conflicts
Remember from earlier:

1. Keep Calm
2. use `git status`
3. follow the instructions

git adds conflict-resolution markers

    <<<<<<< master
    ...
    =======
    ...
    >>>>>>> branch

resolve these manually and mark as resolved with `git add`

# Even more advanced
## 
\bigtext{Even more advanced ideas}

##
\bigtext{This is meant to be informative more so, than something you need to know}

## Git Worktree
Remember git stash? We can do better.

### Solution:
    $ git worktree add <path>          # add a worktree at PATH
    $ git worktree add <path> <branch> # optionally checking out a specific branch
    $ git worktree list                # list active worktrees
    $ git worktree remove <worktree>   # remove non-primary worktree
    $ git worktree move <worktree> <path> # move worktree
    $ git worktree repair <worktree>   # repair worktree

## Partial staging

### Problem
Sometimes we make two different changes to a file, that we'd actually like to put in separate commits.

### Solution
Git uses so-called hunks to store changes and we can store them individually

    $ git add -p    # interactively stage only parts of a file

### Attention
This will only work for changes, that do not lie on the same line.
For changes in adjacent lines we'll need to enter hunk edit mode


## be selective
Selectively apply commits

    git cherry-pick <commit>    # apply changes introduced by <commit>

* get commits from different branches
* reapply an older change

# Outlook

## Where to Learn More
* look at git help pages

        git --help              # display help for git
        git [command] --help    # diplay help page for specific command

* https://git-scm.com/book/en/v2 (the Git Book)
* http://cs.stanford.edu/~blynn/gitmagic/ (full tutorial)

## Text Based Formats work well with Git!
* .tex (\LaTeX)
* .md (Markdown)
* .csv (Tables)
* .svg (Vector Graphics)
* .html
* Any Source Code files

## Git Hosting providers
* [ETH gitlab](https://gitlab.ethz.ch) git hosting provided by ETH.
* [Github.com](https://github.com) biggest platform. run by Microsoft.
* [codeberg.org](https://codeberg.org) non-commercial platform for open-source code
* [Gitlab.com](https://gitlab.com) open-source alternative for bigger projects and enterprise
* [Gitea.com](https://gitea.com) open-source git provider for self-hosting

## IDE support
* vscodium/vscode
* vim-fugitive (vim/neovim)
* magit (emacs)
* JetBrains IDEs
* Eclipse
* lazygit (CLI tool)

##
\bigtext{I hope you learned a lot and thank you for coming!}
