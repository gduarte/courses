# Crashcourse personal/course Website
This is a crash course into web development. Do you want to create your own personal (research?) webpage, customize a theme, or event want to become a web developer? This will help you to get started.

We will introduce HTML, CSS, JavaScript and PHP and add some design / user experience on the way. You need no prior knowledge, and need not to be a programmer. After the course, you can get started with your simple research webpage, or embark on the journey to become a web developer.

## Inhalt
This course will give you a basic understanding of everything you need to create websites. 

Chapters:
- HTML: Structure your content.
- CSS: Design.
- JavaScript: Functionality in the browser.
- PHP: Generate content on the server.

For each chapter, we will see easy applications for personal webpage concepts. Further, we will quickly discuss resources & technologies for aspiring developers.