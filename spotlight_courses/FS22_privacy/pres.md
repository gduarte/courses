---
author:
- Noah Marti
title: Privacy
---

# Overview

## Table of Contents

### Contents

* A short history of privacy
* Arguments for privacy
* Implement privacy on your own
* Discussion

# A short history of privacy

## Anecdotes

### MLK suicide letter

Martin Luther King Jr.

* Leader of the american civil rights movement
	* End discrimination against african americans in the U.S.

FBI

* Break up his marriage
* Wanted the leaders to be at odds with each other
* Send "ominous" threats

### MLK suicide letter

"King, look into your heart. You know you are a complete fraud and a great liability to all us Negroes.”

“We will now have to depend on our older leaders like Wilkins, a man of character and thank God we have others like him. But you are done.” 

### MLK suicide letter

"No person can overcome the facts, not even a fraud like yourself. Lend your sexually psychotic ear to the enclosure. You will find yourself and in all your dirt, filth, evil and moronic talk exposed on the record for all time. . . . Listen to yourself, you filthy, abnormal animal. You are on the record."

### MLK suicide letter

"King, there is only one thing left for you to do. You know what it is. You have just 34 days in which to do it. You are done. *There is but one way out for you.* You better take it before your filthy, abnormal fraudulent self is bared to the nation."

\soft{Source: https://www.eff.org/deeplinks/2014/11/fbis-suicide-letter-dr-martin-luther-king-jr-and-dangers-unchecked-surveillance}

### Target knows you're pregnant

* ID number for every customer
	* Mail, credit card, name
* Pregnancy prediction score
	* Unscented lotion, nutrition supplements
	* Predict due date

### Target knows your pregnant

Take a fictional Target shopper named Jenny Ward, who is 23, lives in Atlanta and in March bought cocoa-butter lotion, a purse large enough to double as a diaper bag, zinc and magnesium supplements and a bright blue rug. There’s, say, an *87 percent chance that she’s pregnant* and that her *delivery date is sometime in late August.*

\soft{Source: https://www.forbes.com/sites/kashmirhill/2012/02/16/how-target-figured-out-a-teen-girl-was-pregnant-before-her-father-did/?sh=2c8ff2e66686}

### Cambridge Analytica

The Company

* subsidary of the private intelligence company SCL group
	* Ties to: British Conservative Party, British royalty, British military
*  misappropriation of digital assets, data mining, data brokerage, data analysis during electoral processes
* Donald Trumps and Ted Cruz's presidential campaign, brexit
* 'War on Terror' Propaganda

### Cambridge Analytica

The Scandal

*  CEO Alexander James Ashburner Nix boasted about: 
	* using prostitutes, bribery sting operations, and honey traps to discredit oppositional politicians
	* that the company "ran all of (Donald Trump's) digital campaign"
* Data of up to 87 million Facebook users were aquired
* US sensitive polling and election data were passed to Russian Intelligence

### Cambridge Analytica

The Aftermath

* closed in 2018 (insolvency)
* Several new firms with former officials
	* Influencing politics and society in Africa and Middle East
	* still working for governments

### Cambridge Analytica

The Methods

* collect data
* create psychological profiles
* "influence their attitudes, emotions or behaviors through psychologically informed interventions at scale"

"Today in the United States we have somewhere close to four or five thousand data points on every individual ... So we model the personality of every adult across the United States, some 230 million people." - CA Chief Executive

### Cambridge Analytica

The Effect

* Moderate to Strong

\soft{Source: https://en.wikipedia.org/wiki/Cambridge\_Analytica}

### Fichen Affäre

ThIs wOuLd nEvEr hApPeN In sWiTzErLaNd

Yes. Yes it absolutely would.

### Fichen Affäre

What happened?

* Parlamentarische Untersuchungskomission (parliamentary investigation commitee)
* Observation of citizen through the police
* Over 900'000 "Fichen"

\soft{Source: https://de.wikipedia.org/wiki/Fichenskandal}

\soft{Source: https://fichen.ch/}

### Fichen Affäre

\includegraphics[height=0.8\textheight]{img/fichefrisch.png}

### Fichen Affäre

\includegraphics[height=0.8\textheight]{img/fichefrisch2.png}

### Fichen Affäre

\includegraphics[height=0.8\textheight]{img/fiche3.jpg}

# Arguments for privacy

## Arguments

### Privacy is a human right

\bigtext{Privacy is a human right}

### You are not immune to propaganda

\includegraphics[height=0.8\textheight]{img/garfield_you_are_not_immune_to_propaganda.png}

### Self censoring

\bigtext{You are prone to self censoring}

### What is illegal in the future?

\bigtext{You don't know what is illegal in 20 years}

### Online privacy is no different to offline privacy

* What are your facial expressions when you sit on the toilet?
* Where did your skin itch when you went to the doctor?
* How cringy was the start and end of your last romantic encounter?
* How does your underwear drawer look like?

Only your business and noone's else

# Do it yourself - privacy

## About

### What is this part of the presentation about?

#### Yes

* Protecting yourself from privacy abusing services

#### No

* Protecting you from hackers
* Protecting you from intelligence services

## General

### Services not to use

* Google
* Facebook
* Microsoft
* Amazon
* ...

### General Advice

\bigtext{If the service is free YOU are the product}

### General Advice - Terms and conditions may apply

#### Fortunately not always the case...

* Free and Open Source Software (FOSS)
* For example: GNU/Linux
* Some companies rely on: donations, unpersonalized ads, community

#### ... but ...

* Just because you pay doesn't mean you don't get tracked (Windows)

## Browsing the web

### Browsers

Browsers:

There are infinitely many

* Firefox
* Chromium
* GNU IceCat
* Brave

### Browser comparison

ungoogled-chromium

* chromium: open source project behind chrome
* removed dependencies from google web services
* still depends on google
* github.com/Eloston/ungoogled-chromium

### Browser comparison

Firefox

* open source/free software license
* doesn't track you (that much)
* a lot of add-ons

### Browser comparison

GNU IceCat

* Ice isn't Fire and a Cat isn't a Fox
* modified Firefox
* fully free software
* fewer add-ons

### Browser comparison

Brave

* built in ad & tracker blocking
* depends on chromium
* Mozilla Public License

### Search engines

* DuckDuckGo
* searX (Meta search engine)

### Add-ons

Firefox Add-ons

* uBlock Origin
* Privacy Possum
* random user agent
* uMatrix

## Network

### DNS-Server

What is a DNS server

* Dynamic Name System
* resolves an IP from an address
	* thealternative.ch 192.33.91.26

Recommended

* Mullvad DNS
* A Pi-hole
* (self host)

### Router

Hardware:

* usually depends on your ISP
* own router via bridging
* everything with (W)LAN connectivity

Operating System:

* OpenWrt
	* openwrt.org

### Pi-hole

* Network wide Ad-blocking
* can be used as dhcpc server
* EUPL (EU Public License)
* Source code on github

### Tor

What is Tor?

* We believe everyone should be able to explore the internet with privacy. We are the Tor Project, an [...] US nonprofit. We advance human rights and defend your privacy online through free software and open networks.

### Tor

\includegraphics[height=0.8\textheight]{img/tor-working.png}

\soft{Source: https://fossbytes.com/everything-tor-tor-tor-works/}

### Tor

How does Tor work?

* Normal: your PC -> Website
* Tor: your PC -> Node1 -> Node2 -> Node3 -> Website
* Every node adds an encryption layer

### Tor

What can I use Tor for?

Yes:

* stay anonymous

No:

* Most likely not safe for illegal stuff
* CIA honeypot?

### Tor

#### Additional literature

* https://mascherari.press/onionscan-report-april-2016-the-tor-network-security-and-crime/

* https://mascherari.press/onionscan-report-fhii-a-new-map-and-the-future/


## Spyware alternatives

### Messengers

* WhatsApp
* Telegram
* Signal
* Matrix
* Threema

### Messenger Comparison

Whatsapp

* Made by Facebook

### Messenger Comparison

Telegram

* Open Source Client
* Server not Open Source
* Financing
	* CEO Pavel Durov
	* Premium users
	* One-to-many channels with ads
* Allows secret chats

\soft{telegram.org/faq}

### Messenger Comparison

Telegram - Pavel Durov

* Rich
* Has some interesting ideas
* Critical of the russian regime
* t.me/durov

\includegraphics[height=0.8\textheight]{img/paveldurov.jpeg}

### Messenger Comparison

Matrix

* Matrix is an open source project that publishes the Matrix open standard for secure, decentralised, real-time communication, and its Apache licensed reference implementations.
* Both client(s) and server are open source (Apache License)
* Decentralized
* E2E encryption
* No number required
* A lot of features

\soft{matrix.org}

### Messenger Comparison

Matrix - Federation

\includegraphics[height=0.8\textheight]{img/matrixfederation.png}

### Messenger Comparison

Signal

* Both client and server are open source (GNU Affero License)
* E2E encryption
* Easy to use

\includegraphics[height=0.2\textheight]{img/signalsnowden.png}

\soft{signal.org}

### Messenger Comparison

Threema

* The messenger that puts security and privacy first
* Swiss made
* E2E encryption
* No phone number required
* Paid
* threema.ch/en/messenger-comparison

\soft{threema.ch}

### Messenger Comparison

#### Additional Literature

* Session
	* https://getsession.org/

### Mail

* Protonmail
* self host

### Payment Methods

* Credit Cards / e-banking
	* Whatever the banks are doing
* Cryptocurrency
	* pseudonym
	* "let's produce a few tons of $CO_2$ to transfer 2.50.-"
* Cash
	* "anonymous"

### App store

Fdroid (Google Play replacement)

* FOSS apps
* Warns you from undesired attributes
	* "advertises nonfree network services"
* A lot of good replacements

### Smartphone - Hardware alternatives

* PinePhone (pine64.org)
* Librem 5 (puri.sm)

### Smartphone - OS alternatives

* LineageOS
	* A free and open-source operating system for various devices, based on the Android mobile platform.
* GrapheneOS (only Pixel devices)
	* GrapheneOS is a privacy and security focused mobile OS with Android app compatibility developed as a non-profit open source project.
* /e/os
	* We build desirable, open source, privacy-enabled smartphone operating systems.

### Maps

* openstreetmap.org
* geo.admin.ch

### Internet Videos

* invidio.us
* youtube-dl
* NewPipe
* PeerTube

### Video/Voice chat

* Jitsi
* Mumble

### Office Applications

* LibreOffice
* Openoffice

### Operating System

* GNU/Linux
* BSD
* TempleOS (Networking is of the devil anyways)

###  More

Work in progress

* Cloud Storage: Nextcloud

# Discussion

## Discussion

### Discuss with others

* What are you doing to protect your privacy?
* Where could you protect your privacy more?
* Have you found any interesting alternatives for "spyware"?
* Which "spyware" are you still using and would like to replace?

*please leave some feedback under: feedback.thealternative.ch/privacy*

### Course material
* These slides: \
    \soft{http://thealternative.ch}

<!-- -->

* \soft{Theme by} Christian Horea, [CC BY](https://creativecommons.org/licenses/by/4.0/)
* \soft{Original Presentation by} Noah Marti
