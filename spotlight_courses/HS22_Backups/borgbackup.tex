\documentclass[aspectratio=1610,handout]{beamer}  % Remove handout to generate pdf with overlays
\usetheme[titleformat=regular,progressbar=head,background=light,block=fill]{metropolis}

\usepackage{hyperref}
\usepackage{booktabs}
\usepackage[normalem]{ulem}
\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\fontsize{11}{13}\selectfont\ttfamily,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
\lstset{style=mystyle}
\lstdefinelanguage{Ini}
{
    columns=fullflexible,
    morecomment=[s][\color{codepurple}\bfseries]{[}{]},
    morecomment=[l]{\#},
    morecomment=[l]{;},
    commentstyle=\color{codegreen}\ttfamily,
    morekeywords={},
    otherkeywords={=,:},
    keywordstyle={\color{magenta}\bfseries}
}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title[short title]{Backups in GNU/Linux}
\subtitle{Planning and automating backups with Borg}
\author{Iyán Méndez Veiga}
\date{November 18, 2022}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------

\begin{document}

\begin{frame}
\titlepage
\end{frame}

%------------------------------------------------

\begin{frame}{Plan for this spotlight}
\begin{enumerate}
 \item<1-> Why backups?
 \item<2-> Planning backups
 \item<3-> Efficient backups with Borg
 \item<4-> (Other tools)
\end{enumerate}
\end{frame}

%------------------------------------------------
\section{Why backups?}
%------------------------------------------------

\begin{frame}{Why backups?}
\begin{itemize}
 \item<1-> \alert{Information age}: we generate and consume huge amounts of digital data
 \item<2-> This number continues to grow year after year ($\sim 60$ ZB in 2020)
 \item<3-> We depend on this data in our daily life
 \item<4-> We invest a lot of \alert{time and effort} creating (some of) this data
 \item<5-> \alert{Murphy's law}: anything that can happen, will eventually happen\\
 \onslide<6->{$\rightarrow$ And loosing our valuable data \underline{can} indeed happen}
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}{What can actually happen?}
\onslide<1->{Many things can go wrong.}

\onslide<2->{From more to less likely (not rigorously)}
\begin{itemize}
 \item<3-> \alert{Human error}
 \item<4-> Hardware failure
 \item<5-> Accidents
 \item<6-> Theft
 \item<7-> Malware
 \item<8-> Natural disasters
 \item<9-> \dots
\end{itemize}

\onslide<10->{$\sim90\%$ of times backups will save us from our own mistakes!}
\end{frame}

%------------------------------------------------

\begin{frame}{However...}
\onslide<1->{\alert{Opportunity cost!} Backups are \underline{not} free. We need:}
\begin{itemize}
 \item<2-> Time to plan and execute them
 \item<3-> Time to test them
 \item<4-> Additional resources (storage, computation time, bandwidth, ...)
 \item<5-> \$\$\$ for third-party offsite backups
\end{itemize}
\onslide<6->{Two takeaways:}
\begin{enumerate}
 \item<7-> Plan and automate your backups to minimize time, effort and costs
 \item<8-> Don't back up \emph{rubbish} to minimize resources
\end{enumerate}
\end{frame}

%------------------------------------------------

\begin{frame}{What is a backup?}
\begin{itemize}
 \item<1-> There are many valid definitions out there
 \item<2-> But this is what I mean by \textbf{backup} from now on
\end{itemize}
\begin{exampleblock}<3->{Backup}
An \alert{explicit and exact copy} of some data, optionally compressed and/or encrypted, with \alert{several snapshots} corresponding to different points in time that are \alert{easily accessible}
\end{exampleblock}
\begin{itemize}
 \item<4-> A simple mirror is not a backup \onslide<5->{{\tiny(no snapshots $\rightarrow$ mistakes will propagate)}}
 \item<6-> Storage redundancy (e.g. RAID5) is also not a backup \onslide<7->{{\tiny(no explicit copy and probably no snapshots)}}
 \item<8-> A read-only copy of our data with snapshots every day for the last 10 years that we don't know how to restore is definitely not a backup \onslide<9->{{\tiny(not easily accessible)}}
 \item<10-> Most cloud providers don't offer you backups (e.g. GoPro cloud)
\end{itemize}
\onslide<11->{About last property: What is worse than not having a backup?}\\
\onslide<12->{$\rightarrow$ Thinking you have one, only to learn when you need it that you cannot use it}
\end{frame}

%------------------------------------------------

\begin{frame}{Types of backup}
\onslide<1->{There are different ways to categorize backups}
\begin{itemize}
 \item<2-> Full system backups (system images) vs \alert{data backups}
 \item<3-> Full vs incremental vs differential
 \item<4-> Discrete vs continuous backups
 \item<5-> Onsite vs offsite backups
 \item<6-> \dots
\end{itemize}
\onslide<7->{Today we will focus on doing discrete data backups with Borg.}\\
\onslide<8->{Technically we will do one full backup and later as many incremental/differential as we want}
\end{frame}

%------------------------------------------------
\section{Planning backups}
%------------------------------------------------

\begin{frame}{General ideas}
\begin{itemize}
 \item<1-> If you care about cybersecurity, backups are a must
 \item<2-> When you doubt if you are backing up often enough, probably you aren't
 \item<3-> Don't think of backups as being there for you \alert{\emph{if}} you ever lose data\\
 $\rightarrow$ Think of them being there for you \alert{\emph{when}} you lose your data
 \item<4-> Don't forget Murphy's law\\
 $\rightarrow$ The more you use electronic devices regularly, the more likely you will lose some data eventually
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}{Proposal to categorize your data}
\onslide<1->{Before using any backup tool, we need to \alert{understand our own data}.}\\
\onslide<2->{The first step is to categorize the data based on how important it is to us}
\begin{itemize}
 \item<3-> \textbf{Irreplaceable}: \onslide<4->{family photos \& videos, encryption keys...}
 \item<5-> \alert{\textbf{Very important}}: \onslide<6->{\LaTeX\;source of your PhD thesis...}
 \item<7-> \alert{Important}: \onslide<8->{contracts, bills, some emails...}
 \item<9-> Dispensable: \onslide<10->{games, movies, music...}
 \item<11-> \sout{Garbage}: \onslide<12->{caches, spam, screenshots...}
\end{itemize}
\onslide<13->{\textbf{Exercise}: make a table with all your data and assign each item a category.\\
Add additional columns if you think it can help you (e.g. size, how often is modified, other ``backup'', device, \dots).}\\
\onslide<14->{Useful commands: \texttt{du -sh}, \texttt{find -mtime -1}}\\
\onslide<15->{Useful apps: Filelight}
\end{frame}

%------------------------------------------------

\begin{frame}{Example table}
\begin{table}
    \begin{tabular}{lccl}
      \toprule
      Data & Category & Size (GiB) & Modified\\
      \midrule
      /home/iyan/.gnupg & Irreplaceable & 0 & Rarely\\
      /home/iyan/Documents & Very important & 132 & Daily\\
      /home/iyan/Pictures & Irreplaceable & 458 & Rarely\\
      /home/iyan/Music & Dispensable & 2538 & Very rarely\\
      Phone (2FA codes, Signal, etc.) & Important & 1 & Daily\\
      me@iyanmv.com & Very important & 2 & Daily\\
      \multicolumn{1}{c}{\vdots} & \vdots & \vdots & \vdots\\
      \bottomrule
    \end{tabular}
  \end{table}
\end{frame}

%------------------------------------------------

\begin{frame}{Backup frequency}
\begin{itemize}
 \item<1-> Different data is modified at different pace
 \item<2-> With typical backup tools we should optimize this to avoid wasting storage\\
 $\rightarrow$ e.g. Increase time between differential backups
 \item<3-> With Borg not that important because of the efficient \alert{deduplication}
 \item<4-> For most people, \alert{one backup per day} should be enough
 \item<5-> If you have data that is both irreplaceable and updated very often (e.g. your current work project), you can consider backing that folder every few hours
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}{The 3-2-1 strategy}
\begin{itemize}
 \item<2-> Similar to investing, \emph{diversify} your backups
 \item<3-> At least \alert{3 copies} of your data
 \item<4-> \alert{2 local} (on-site) but on different devices {\tiny(external HDD, NAS, ...)}
 \item<5-> At least \alert{1 off-site} copy {\tiny(cloud provider, friend's computer, ...)}
 \item<6-> Follow this rule at least for irreplaceable and very important data
\end{itemize}
\end{frame}

%------------------------------------------------
\section{Backups with Borg}
%------------------------------------------------

\begin{frame}{A little bit about Borg}
\begin{itemize}
 \item<1-> \alert{\textbf{BorgBackup}} (or simply Borg) is a deduplicating backup program
 \item<2-> Written in Python and C (Cython)
 \item<3-> It is free software (BSD license)
 \item<4-> Officially supports GNU/Linux, macOS and FreeBSD
 \item<5-> Also experimental support for Cygwin and WSL
 \item<6-> Space efficient storage: chunk-based \alert{deduplication}
 \item<7-> Supports compression: lz4, zlib, lzma, \alert{zstd}
 \item<8-> Supports (authenticated) encryption: AES256
 \item<9-> Supports efficient offsite backups: delta transfers via SSH
 \item<10-> Big changes coming soon with Borg 2.x (crypto, repos, compression)
\end{itemize}
\begin{center}
\onslide<11->{\href{https://www.borgbackup.org/}{\alert{https://www.borgbackup.org}}\\
\href{https://borgbackup.readthedocs.io}{\alert{https://borgbackup.readthedocs.io}}}

\end{center}
\end{frame}

%------------------------------------------------

\begin{frame}{Who uses Borg?}
\begin{itemize}
 \item<1-> FSFE
 \item<2-> Archlinux
 \item<3-> ...
 \item<4-> You?
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}{How can I install Borg?}
\onslide<1->{Easy! Almost all distros have Borg in their repos now}
\begin{itemize}
\item<2-> Debian/Ubuntu: \lstinline|sudo apt-get install borgbackup|
\item<2-> Fedora: \lstinline|sudo dnf install borgbackup|
\item<2-> Arch Linux: \lstinline|sudo pacman -S borg|
\item<2-> Gentoo: \lstinline|emerge borgbackup|
\item<2-> OpenSUSE: \lstinline|sudo zypper in borgbackup|
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}{Attack model}
\begin{itemize}
 \item<1-> Environment of the \alert{client process is trusted}
 \item<2-> Repository (server) is not trusted
 \item<3-> The attacker has any and all access to the repository, including interactive manipulation (man-in-the-middle) for remote repositories
 \item<4-> Under this mode, Borg guarantees that an attacker cannot:
 \begin{enumerate}
  \item<5-> Modify the data of any archive without the client detecting the change
  \item<6-> Rename, remove or add an archive without the client detecting the change
  \item<7-> Recover plain-text data
  \item<8-> Recover definite (heuristics based on access patterns are possible) structural information such as the object graph (which archives refer to what chunks)
 \end{enumerate}
 \item<9-> The attacker can always impose a denial of service
 \item<10-> Borg fails to provide confidentiality if multiple clients independently update the same repository
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Commands we will see today}
\onslide<1->{We always run Borg in the same way}
\onslide<2->\begin{lstlisting}
borg [common options] command [options] ARCHIVE [PATH...]
\end{lstlisting}
\onslide<3->{With common options we can control how the output looks (e.g. enable debug messages), set a network upload rate limit, etc.}

\onslide<4->{These are the commands we will see today:}
{\tiny
\begin{itemize}
 \item<4-> \lstinline|init|
 \item<4-> \lstinline|create|
 \item<4-> \lstinline|info|
 \item<4-> \lstinline|list|
 \item<4-> \lstinline|prune|
 \item<4-> \lstinline|compact|
 \item<4-> \lstinline|check|
 \item<4-> \lstinline|mount|
\end{itemize}}

\onslide<5->{Once we see how to use these, you can check the docs and explore the remaining commnands}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Creating our first backup with Borg}
\onslide<1->{The first thing we need to do is initialize a repository.}\\
\onslide<2->{This is the directory where Borg will store all the deduplicated data, some metadata and your (encrypted) encryption keys}
\onslide<3->\begin{lstlisting}
borg init --encryption MODE \
          --append-only \
          --storage-quota QUOTA \
          --make-parent-dirs /path/to/repo
\end{lstlisting}
\onslide<4->{The encryption MODE can be: none, authenticated, authenticated-blake2, repokey, keyfile, repokey-blake2 or keyfile-blake2.\\ \alert{Note}: The MODE can only be configured when creating a new repository.}

\onslide<5->{The QUOTA (e.g. 1.5T, 500G) can be changed later. For example, to remove any limit}
\onslide<6->\begin{lstlisting}
borg config /path/to/repo storage_quota 0
\end{lstlisting}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Creating our first backup with Borg}
\onslide<1->{Once we have initialized a repository, we can create our first backup}
\onslide<2->\begin{lstlisting}
borg create /path/to/repo::ARCHIVE-NAME path/to/files
\end{lstlisting}
\begin{itemize}
 \item<3-> The archive name must be unique
 \item<4-> Borg defines some useful variables (e.g. \{now\}, \{hostname\}, \{user\}, ...)
 \item<5-> Some examples:
\onslide<6->\begin{lstlisting}
borg create /path/to/repo::my-documents ~/Documents
borg create --compression auto,lzma,9 \
            /path/to/repo::desktop ~/Desktop
borg create ssh://borg@backup.example.org:2222\
            /path/to/repo::{fqdn}-home-{now} /home
\end{lstlisting}
\item<7-> Many useful specific options $\rightarrow$ Check the docs!
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}{And our second, third...}
\begin{itemize}
 \item<1-> Try to run a few times the same create command
 \item<2-> Change filenames, move files around, etc.
 \item<3-> Modify binary files
 \item<4-> etc.
\end{itemize}
\onslide<5->{New backups only include chunks that are not already in the repository.}\\
\onslide<6->{$\rightarrow$ We avoid having duplicates $\Rightarrow$ \alert{Deduplication}}
\end{frame}

%------------------------------------------------

\begin{frame}{Deduplication?}
\begin{itemize}
 \item<1-> Deduplication is a method to save storage at the cost of some additional computation (e.g. computing and checking hashes, instead of just checking filenames and modification times)
 \item<2-> Borg does a \alert{chunk-based deduplication} (way more efficient than file-based)
 \item<3-> Each file is split into a number of variable length chunks
 \item<4-> Only chunks that have never been seen before are added to the repository
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Querying information about our repo}
\begin{lstlisting}
borg info /path/to/repo
borg --last N /path/to/repo
borg info /path/to/repo::archive
\end{lstlisting}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Listing archives in a repo}
\begin{lstlisting}
borg list /path/to/repo
\end{lstlisting}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Pruning}
\begin{itemize}
 \item<1-> If storage is limited, we will run out of space eventually
 \item<2-> It's nice to have tons of snapshots, but do we really need them?
 \item<3-> A good compromise is to define a \alert{retention policy}
 \item<4-> For example, we can decide to keep one snapshot per day for the last two weeks, one snapshot per week for a month, and beyond that only one snapshot per month
 \item<6-> All this can be achieved with \lstinline|prune| (enforce retencion policy) and \lstinline|compact| (free space) commands
\end{itemize}
\onslide<5->\begin{lstlisting}
borg prune --keep-daily 14 --keep-weekly 4 --keep-monthly -1
\end{lstlisting}
\onslide<7->\begin{lstlisting}
borg --progress compact --cleanup-commits /path/to/repo
\end{lstlisting}
\onslide<8->{\alert{Note}: Compactation is only possible if the the (local) repository is not append only. Cloud providers may give you the option to compact repos automatically and connect with append-only users.}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Checking integrity}
\begin{itemize}
 \item<1-> It is a good idea to verify the integrity of our repository once in a while (e.g. once per month)
 \item<2-> Errors can happen due to unreliability hardware (e.g. bad memory)
 \item<3-> Errors detected early can be fixed
 \item<4-> If we never check/correct errors, these will propagate and may lead to data loss eventually
 \item<5-> It is good practice to \alert{check and compact} repos before and after a \alert{major Borg update}
\end{itemize}
\onslide<6->\begin{lstlisting}
borg --progress --verbose check /path/to/repo
borg --progress --verbose check --verify-data /path/to/repo
borg --progress --verbose check --repair /path/to/repo
\end{lstlisting}
\onslide<7->{\alert{Note}: Repair flag can result in data loss if errors can't be fixed. Do a backup of your repo (\texttt{borg export-tar}) and ask for help (IRC, email list) before trying anything if you don't understand what you are doing.}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Restoring data from backups}
\begin{itemize}
 \item<1-> Remember: it is not a backup if we cannot access the snapshots easily
 \item<2-> Two commands to restore data from a Borg repository
 \begin{enumerate}
  \item<3-> \lstinline|mount|: convenient to explore your snapshots but slow
  \item<5-> \lstinline|extract|: fast and efficient if you know exactly what you want
 \end{enumerate}
\end{itemize}
\onslide<4->\begin{lstlisting}
borg mount /path/to/repo /mnt/borg_restore
cd /mnt/borg_restore
borg umount /mnt/borg_restore
\end{lstlisting}
\onslide<6->\begin{lstlisting}
mkdir borg_restore
cd borg_restore
borg list /path/to/repo
borg extract /path/to/repo::ARCHIVE path/to/extract
\end{lstlisting}
\end{frame}

%------------------------------------------------

\begin{frame}{Final comments about Borg}
\begin{itemize}
 \item<1-> Careful with running out of space\\
 $\rightarrow$ Either \alert{configure quotas} or use LVM logical volumes
 \item<2-> To avoid permission issues, \alert{always access the repository using the same user account}
 \item<3-> Run with an unprivileged user whenever is possible
 \item<4-> Careful with files changing during the creation of an archive (e.g. databases, VMs, containers)
 \item<5-> \alert{Block automatic updates} (at least major ones) from your distribution and read the release notes before manually updating and creating a new archive
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}{Cloud providers compatible with Borg}
\begin{itemize}
 \item<2-> \alert{BorgBase}: 10 GiB free, \$24/year 100 GiB, \$80/year 1 TiB, \$150/year 2 TiB
 \item<3-> rsync.net: \$18/year 100 GiB, \$96/year 1 TiB (up to 99 TiB: 0.8 ct / GiB / Month)
 \item<4-> Hetzner: 46€/year 1TiB, 156€/year 5 TiB
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}{Automating everything}
The goal is to set up this once and then forget about it.
\begin{itemize}
 \item<2-> Two scripts: one to create and prune backups, and one to check them
 \item<3-> Two simple systemd services
 \item<4-> Two systemd timers
\end{itemize}
\onslide<5->{I've been using this setup for almost three years now, and no issues so far.}
\end{frame}

%------------------------------------------------

\begin{frame}[allowframebreaks]{Script (create off-site backups)}
\lstinputlisting[language=Bash,basicstyle=\fontsize{7}{9}\selectfont\ttfamily]{files/borg-create.sh}
\end{frame}

%------------------------------------------------

\begin{frame}[allowframebreaks]{Script (check off-site backups)}
\lstinputlisting[language=Bash,basicstyle=\fontsize{7}{9}\selectfont\ttfamily]{files/borg-check.sh}
\end{frame}

%------------------------------------------------

\begin{frame}{Systemd services and timers}
\lstinputlisting[language=Ini,basicstyle=\fontsize{7}{9}\selectfont\ttfamily]{files/borg.service}
\lstinputlisting[language=Ini,basicstyle=\fontsize{7}{9}\selectfont\ttfamily]{files/borg.timer}
\end{frame}

%------------------------------------------------

\begin{frame}{Systemd services and timers}
\lstinputlisting[language=Ini,basicstyle=\fontsize{7}{9}\selectfont\ttfamily]{files/borg-check.service}
\lstinputlisting[language=Ini,basicstyle=\fontsize{7}{9}\selectfont\ttfamily]{files/borg-check.timer}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]{Installing and enabling systemd units}
\begin{enumerate}
 \item<1-> Copy scripts
 \item<2-> Adapt them to your needs
 \item<3-> Copy systemd units to \texttt{~/.config/systemd/user}
 \item<4-> Edit them
 \item<5-> Enabled them with \texttt{s}
 \onslide<6->\begin{lstlisting}
systemctl --user enable borg.timer --now
systemctl --user enable borg-check.timer --now
\end{lstlisting}
\end{enumerate}
\end{frame}

%------------------------------------------------

\begin{frame}{Syncing for centralized backups}
\begin{itemize}
 \item<1-> You can use borg on multiple devices all accessing the same repository
 \item<2-> You can use the \texttt{\{hostname\}} or \texttt{\{fqdn\}} variables to create archives
 \item<3-> But you can also synchronize (e.g. with syncthing) your devices and always run Borg on the same device
\end{itemize}
\end{frame}

%------------------------------------------------
\section{Other tools}
%------------------------------------------------

\begin{frame}{Other tools}
Borg is awesome but there are other great tools out there
\begin{itemize}
 \item<1-> borgmatic
 \item<2-> restic
 \item<3-> bacula
 \item<4-> bup
 \item<5-> ...
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}[standout]
Thank you! Question?
\end{frame}

\end{document}
