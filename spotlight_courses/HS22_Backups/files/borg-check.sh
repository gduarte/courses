#!/bin/bash -ue

# Colors
NC='\e[0m'
GR='\e[1;32m'
SP='\e[105m'

# This is the location of the BorgBase repository
TARGET=xxx.repo.borgbase.com:repo

# Options for borg create
BORG_OPTS="-v"

# Repo key passphrase
export BORG_PASSPHRASE="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

# It is better to just fail quickly instead of hanging.
export BORG_RELOCATED_REPO_ACCESS_IS_OK=no
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=no

# Log Borg version
echo -e "${SP}$(borg --version)${NC}"

echo -e "${GR}$(date '+%Y-%m-%d %H:%M')${NC} Starting to check repo"

borg check $BORG_OPTS $TARGET \
2>&1 | tee ~/.backup_check_last.log

echo -e "${GR}$(date '+%Y-%m-%d %H:%M')${NC} Check completed"

cat <<EOF > ~/.backup_check_last.mail
To: iyanmv@gmail.com
From: Borg <me@iyanmv.com>
Subject: [BorgBase] Check report $(date '+%Y-%m-%d %H:%M')

EOF
cat ~/.backup_check_last.log >> ~/.backup_check_last.mail
cat ~/.backup_check_last.mail | msmtp -a iyanmv iyanmv@gmail.com
