#include<pthread.h>

volatile int inc;

void *
thread(void *arg __attribute__((unused))) {
  for (int i = 0; i < 65536; i++) 
	inc++;
  return NULL;
}

int
main(int argc, char **argv) {
  pthread_t t1, t2;
  pthread_create(&t1, NULL, thread, NULL);
  pthread_create(&t2, NULL, thread, NULL);
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  return 0;
}
