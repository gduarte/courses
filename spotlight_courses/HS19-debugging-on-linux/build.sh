#!/bin/bash
printf "Building Main Presentation... "
pandoc -t beamer --template template.tex --listings pres.md -o pres.pdf --pdf-engine pdflatex 
echo "Done"
