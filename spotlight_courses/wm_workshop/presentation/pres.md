---
author:
- Noah Marti
title: Window Manager Workshop
---

# Introduction

## Introduction

### What is this workshop about?

Contents

* An introduction by Noah Marti
* Lukas Tobler about his bar
* YOUR time

### What can you do?

Whatever you want

* Rice your WM/DE on your own
* Rice your WM/DE with our help

# Ricing Guide - Basics

## Do you know the five thinking steps?

### What is ricing?

Why RICE

* Racing Inspired Car Enhancements

Ricing your Computer

* Configuring the graphical part of your OS to suit your desires

### The five steps of ricing

* WM
* Basic config
* Intermdeiate config
* Advanced config
* Finishing touch

### WM

* Decide which WM/DE you want to use

### Basic Configuration

* keybindings
* autostart
* wallpaper
	* image
* lockscreen
	* image
	* generated image

### Intermediate Configuration

* statusbar
* panel
* display on wallpaper
* terminal

### Advanced Configuration

Additional cool tools

* ranger
* rofi
* dmenu

* Just for fun
	* neofetch (or similar)
	* cmatrix
	* pipes.sh

### Finishing Touch

* Configure your day-to-day programs

# Ricing Guide - Details

## Window Manager

### Window Manager (or DE)

* floating/tiling/dynamic
* have a look at the arch wiki [https://wiki.archlinux.org/title/Window\_manager]
	* just test every WM :^)
* what language/config do they use?
* have a look at their homepage
* look at some screenshots in the web
* openbox, fvwm, berrywm, i3, herbstluftwm, bspwm, eiswm

## Basic

### Keybindings

* usually comes with your WM
* sxhkd

### Autostart

* usually comes with your WM

### Wallpaper & Lockscreen

* Wallpaper
	* Any image you like
	* Set with feh, nitrogen, bgs, hsetroot, habak, display
	* Derive a colorscheme from it
* Lockscreen
	* Program to lock screen
	* Image
	* Generate Image with a skript (ImageMagick)

## Intermediate

### Statusbar

* Polybar
* Lemonbar
* dzen2 (write your own)

### Panel

* xfce4-panel
* tint2

### Info on wallpaper

* conky 

### Terminal

* Whatever you like
* What do you want your terminal to support?
* Shell
	* bash (standard)
	* csh, fsh, ...

## Advanced

### Additional things to have a look at

* neofetch
	* display system specifications in terminal
* redshift
	* "night mode" for your screen
	* alternative: xrandr gamma correction

* pywal
	* create and apply colorscheme

### Additional things to have a look at

* rofi
	* create menues
* dmenu
	* program launcher
* .Xresources
	* [https://wiki.archlinux.org/title/X\_resources]
	* configure parameters for X client applications

# Examples

## Some examples

### i3 + i3blocks

\includegraphics[width=0.8\textwidth]{img/jc.png}

### eiswm

\includegraphics[width=0.8\textwidth]{img/eiswm.png}

### berrywm

\includegraphics[width=0.8\textwidth]{img/berrywm.png}

### bspwm

\includegraphics[width=0.8\textwidth]{img/bspwm.png}

## More examples

### Reddit

* unixporn
* search for your specific wm

# Writing your own programs

## Writing your own WM

### Writing your own WM

Dont

### Framework

Graphic Server

* Xorg
* Wayland

### Where to start
 
* from scratch
* start with TinyWM [http://incise.org/tinywm.html]
* fork the WM you like most

### eiswm

* a fork of dwm
	* pretty lightweight
	* written in C
	* patches
* just rename it and pretend its your work :^)
* additional features
	* gaps
	* battery charge
	* time
	* more layouts

## Writing your own bar

### ggbar

\bigtext{ggbar by Lukas Tobler}

## Material

### Course material
* These slides and  additional content: \
    \soft{http://thealternative.ch}

<!-- -->

* \soft{Theme by} Christian Horea, [CC BY](https://creativecommons.org/licenses/by/4.0/)
* \soft{Original Presentation by} Noah Marti
