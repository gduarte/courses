# Courses

This is a public repository that contains a collection of our current (and past) LinuxDays courses.

Introduction:
- [Free and Open Source Software (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/FOSS_course/slides.pdf)
- [Introduction to Linux (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/intro_course/slides.pdf)
- [Install Guide](https://gitlab.ethz.ch/thealternative/courses/-/blob/master/install_guide_updated/master/install_guide_updated.pdf)
- [Bash Guide](https://thealternative.ch/guides/bash.php)
- [Console Toolkit and Linux Toolbox](https://gitlab.ethz.ch/thealternative/courses/-/blob/master/consoletoolkit_linuxtoolbox/pres/pres.pdf)

Console & Bash:
- [Console Toolkit and Linux Toolbox](https://gitlab.ethz.ch/thealternative/courses/-/blob/master/consoletoolkit_linuxtoolbox/pres/pres.pdf)
- [Console Toolkit Exercises (.pdf)](https://gitlab.ethz.ch/thealternative/courses/-/blob/master/archive/ctk_archive/console_toolkit/exercise_files/exercises.pdf)
- [Bash Course (Slides) (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/bash_course/pres/pres.pdf)
- [Bash Course (Guide) (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/bash_course/guide/guide.pdf)
- [Bash Course Exercises (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/bash_course/exercisesheet/sheet.pdf) and [Solutions](https://gitlab.ethz.ch/thealternative/courses/blob/master/bash_course/exercisesheet/sol.pdf)

Tools:
- [Git](https://thealternative.ch/courses/git/git.html)

[Spotlights](https://gitlab.ethz.ch/thealternative/courses/-/tree/master/spotlight_courses):
- FS18 hacking session
- FS22 privacy
- FS23 containerization
- FS23 cryptography
- FS23 introduction to linux
- HS19 debugging on linux
- HS19 scientific software management
- HS20 unix history
- HS20 vim
- HS21 pdf
- HS21 priacy
- HS21 project management
- HS21 web
- HS22 Backups
- HS22 FOSS
- HS22 phone privacy and security
- vimcourse nils 2020
- wm workshop


# Cloning the repository

To download this repository, please install [git-lfs](https://git-lfs.github.com/). You can clone (download) this entire repository by issuing the following command in a terminal:

```
git-lfs install
git clone https://gitlab.ethz.ch/thealternative/courses.git
```

You can find the course materials in the various subdirectories of this repository.

You need `git-lfs` installed for the larger files to download as expected.

# For TheAlternative Members

This repository is kept up to date by TheAlternative members. If you teach a course, please add your slides and supplementary material, such as exercise sheets, to this repo. Do not upload any private files, as this repository is publicly visible.
